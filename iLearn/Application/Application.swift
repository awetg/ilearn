//
//  Application.swift
//  iLearn
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public class Application {
    static let shared = Application()
    
    let langUserDefaultsKey = "lang"
    
    func setLanguage(lang: String) {
        UserDefaults.standard.set(lang, forKey: self.langUserDefaultsKey)
    }
    
    func getLanguage() -> String {
        return UserDefaults.standard.string(forKey: self.langUserDefaultsKey) ?? "en"
    }
    
    func loadFacetValues() {
        
        // download course related facets
        CourseDataManager().search(searchTerm: "*", queryParams: nil) { (courseSearchResults, error) in
            
            if let error = error {
                print(error)
                return
            }
            guard let results = courseSearchResults else { return }
            
            // insert teaching language facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.teachingLangFacet?.facetValues ?? [])
            
            // insert application status facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.appStatusFacet?.facetValues ?? [])
            
            // insert education type facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.edTypeFacet?.facetValues ?? [])
            
            // insert prerequiste facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.prerequisiteFacet?.facetValues ?? [])
            
            // insert topic (education topics) facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.topicFacet?.facetValues ?? [])
            
            // insert form of teaching facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.fotFacet?.facetValues ?? [])
            
            // insert time of teaching facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.timeOfTeachingFacet?.facetValues ?? [])
            
            // insert form of study facet values
            CoreDataManager.shared.insertFacetValues(facetValues: results.formOfStudyFacet?.facetValues ?? [])
            
            CoreDataManager.shared.save()
            
        }
        
        // download provider related facets
        let req = [["text":"*","lang": Application.shared.getLanguage(), "start":"0", "rows":"10","searchType":"PROVIDER"]]
        CourseDataManager().search(searchTerm: "*", queryParams: req) { (providerResults, error) in
            
            if let error = error {
                print(error)
                return
            }
            guard let results = providerResults else { return }
            
            // insert provider type facets
            CoreDataManager.shared.insertFacetValues(facetValues: results.providerTypeFacet?.facetValues ?? [])
            
        }
    }
    
    func reloadFacetValues() {
        CoreDataManager.shared.deleteAllFacetValues()
        self.loadFacetValues()
    }
}
