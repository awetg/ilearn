//
//  Filter.swift
//  iLearn
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public class Filter {
    let title: String
    let facetValues: [FacetValue]
    var selectedOptionId: String
    var filterFacetField: String
    var isExpanded = false
    
    init(title: String, options: [FacetValue], filterFacetField: String, selectedOptionId: String = "") {
        self.title = title
        self.facetValues = options
        self.filterFacetField = filterFacetField
        self.selectedOptionId = selectedOptionId
    }
}
