//
//  SliderItem.swift
//  iLearn
//
//  Created by iosdev on 08/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

public class SliderItem {
    let title: String
    let facetFilter: String
    let image: UIImage
    
    init(title: String, image: UIImage, facetFilter: String) {
        self.title = title
        self.image = image
        self.facetFilter = facetFilter
    }
}
