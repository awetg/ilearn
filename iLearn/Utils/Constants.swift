//
//  Constants.swift
//  iLearn
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

// key value dictionary for facet filter valueId and user data properties
public struct FacetFilterFields {
    static let teachingLang = "teachingLangCode_ffm"
    static let topic = "theme_ffm"
    static let subTopic = "topic_ffm"
    static let educationType = "educationType_ffm"
    static let subEducationType = "educationType_ffm"
    static let timeOfTeaching = "timeOfTeaching_ffm"
    static let formOfTeaching = "formOfTeaching_ffm"
    static let formOfStudy = "formOfStudy_ffm"
    static let all:[String] = [
                      "teachingLangCode_ffm",
                      "theme_ffm",
                      "topic_ffm",
                      "educationType_ffm",
                      "timeOfTeaching_ffm",
                      "formOfTeaching_ffm",
                      "formOfStudy_ffm"]
}

public let FilterTitles:[String: String] = [
    "teachingLangCode_ffm": "Language of instruction",
    "theme_ffm": "Filter by topic",
    "topic_ffm": "Filter by sub topic",
    "educationType_ffm": "Filter by education type",
    "timeOfTeaching_ffm": "Filter by time of teaching",
    "formOfTeaching_ffm": "Filter by form of teaching",
    "formOfStudy_ffm": "Filter by form of study"
]


public let LearningPreferenceTitles:[String: String] = [
    "teachingLangCode_ffm": "Language of instruction",
    "theme_ffm": "Topics",
    "topic_ffm": "Sub topics",
    "educationType_ffm": "Education type",
    "timeOfTeaching_ffm": "Time of teaching",
    "formOfTeaching_ffm": "Form of teaching",
    "formOfStudy_ffm": "Form of study"
]


// topic icons on search view controller, topics are used for filter of course by topic
public let topicIcons: [String: UIImage] = [
    // General education
    "teemat_1": #imageLiteral(resourceName: "school"),
    // Art and Design, Music, Theatre
    "teemat_10": #imageLiteral(resourceName: "paint-palette"),
    // Technology and Engineering
    "teemat_11": #imageLiteral(resourceName: "technology"),
    // Health care, Well-being,…, Medicine and Pharmacy
    "teemat_12": #imageLiteral(resourceName: "stethoscope"),
    // Security, Fire and Rescue Services
    "teemat_13": #imageLiteral(resourceName: "siren-light"),
    // Agriculture, Forestry and Environmental Sciences
    "teemat_14": #imageLiteral(resourceName: "irrigation"),
    // Modern Languages, Inform…Communication and Media
    "teemat_2": #imageLiteral(resourceName: "chat"),
    // History, Folklore and Cultural Studies
    "teemat_3": #imageLiteral(resourceName: "greek-pillar"),
    // Social Sciences and Law
    "teemat_4": #imageLiteral(resourceName: "law"),
    // Business, Economics and Finance
    "teemat_5": #imageLiteral(resourceName: "combo-chart"),
    // Transport Services and administration
    "teemat_6": #imageLiteral(resourceName: "bus"),
    // Natural Sciences
    "teemat_7": #imageLiteral(resourceName: "biotech"),
    // Education, Psychology and other Behavioural Sciences
    "teemat_8": #imageLiteral(resourceName: "psychology"),
    // Tourism, Catering, Domestic services and Consumer economics
    "teemat_9": #imageLiteral(resourceName: "food")
]
