//
//  ILearnError.swift
//  iLearn
//
//  Created by iosdev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public struct ILearnError: Error {
    var message: String
    init(message: String) {
        self.message = message
    }
}

extension ILearnError:  LocalizedError {
    public var errorDescription: String? {
        return message
    }
}
