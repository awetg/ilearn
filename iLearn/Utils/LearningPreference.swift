
import Foundation

public class LearningPreference {
    let title: String
    let facetValues: [FacetValue]
    var selectedIds: [String]
    var isExpanded = false
    
    init(title: String, facetValues: [FacetValue], selectedIds: [String]?) {
        self.title = title
        self.facetValues = facetValues
        self.selectedIds = selectedIds != nil ? selectedIds! : [String]()
    }
}
