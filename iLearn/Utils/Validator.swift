//
//  Validator.swift
//  iLearn
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public protocol Validator {
    func validate(value: String?) throws -> String
}


public enum ValidatorType {
    case email
    // more case e.g username avaialable or not
}

public enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> Validator {
        switch type {
        case .email: return EmailValidator()
        }
    }
}

public class EmailValidator: Validator {
    public func validate(value: String?) throws -> String {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        
        if emailPredicate.evaluate(with: value) {
            return value!
        } else {
            throw ILearnError(message: "Please enter valid email")
        }
    }
}
