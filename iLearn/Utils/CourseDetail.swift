//
//  CourseDetail.swift
//  iLearn
//
//  Created by Viet Tran on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public class CourseDetail {
    
    let asOngoing: Bool
    let credits: String?
    let educationDegree: String?
    let educationDegreeCode: String?
    let educationType: String?
    let homeplace: String?
    let id: String?
    let lopIds: [String]?
    let lopNames: [String]?
    let losId: String?
    let name: String?
    let nextApplicationPeriodStarts: [Int]?
    let parentId: String?
    let prerequisite: String?
    let prerequisiteCode: String?
    let responsibleProvider: String?
    let subjects: [String]?
    let type: String?
    
    init(course: Course) {
         self.asOngoing = course.asOngoing 
         self.credits = course.credits
         self.educationDegree = course.educationDegree
         self.educationDegreeCode = course.educationDegreeCode
         self.educationType = course.educationType
         self.homeplace = course.homeplace
         self.id = course.id
         self.lopIds = course.lopIds
         self.lopNames = course.lopNames
         self.losId = course.losId
         self.name = course.name
         self.nextApplicationPeriodStarts = course.nextApplicationPeriodStarts
         self.parentId = course.parentId
         self.prerequisite = course.prerequisite
         self.prerequisiteCode = course.prerequisiteCode
         self.responsibleProvider = course.responsibleProvider
         self.subjects = course.subjects
         self.type = course.type
        
    }
    
    init(course: CourseDTO) {
        self.asOngoing = course.asOngoing ?? false
        self.credits = course.credits
        self.educationDegree = course.educationDegree
        self.educationDegreeCode = course.educationDegreeCode
        self.educationType = course.educationType
        self.homeplace = course.homeplace
        self.id = course.id
        self.lopIds = course.lopIds
        self.lopNames = course.lopNames
        self.losId = course.losId
        self.name = course.name
        self.nextApplicationPeriodStarts = course.nextApplicationPeriodStarts
        self.parentId = course.parentId
        self.prerequisite = course.prerequisite
        self.prerequisiteCode = course.prerequisiteCode
        self.responsibleProvider = course.responsibleProvider
        self.subjects = course.subjects
        self.type = course.type
        
    }
    
    
}
