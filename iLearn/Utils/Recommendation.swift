//
//  Recommendation.swift
//  iLearn
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

class Recommendation {
    let title: String
    let courses: [Course]
    
    init(title: String, courses: [Course]) {
        self.title = title
        self.courses = courses
    }
}
