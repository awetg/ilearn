//
//  CourseDataManager.swift
//  iLearn
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public protocol CourseDataManagerProtocol {
    
    func search(searchTerm: String, queryParams: [[String:String]]?, completionHandler: @escaping (CourseSearchResultDTO?, Error?) -> ())
}



public class CourseDataManager: CourseDataManagerProtocol {
    
    private let netWorkClient: NetworkClientProtocol
    
    public init(netWorkClient: NetworkClientProtocol) {
        
        self.netWorkClient = netWorkClient
    }
    
    convenience init() {
        self.init(netWorkClient: NetworkClient())
    }
    
    private func networkRequest<T: Decodable>(req: Request, encodeType type: T.Type, completionHandler: @escaping (T?, Error?) -> ()) {
        self.netWorkClient.execute(req) { (response) in
            switch response.result {
            case .success(_):
                do{
                    completionHandler(try response.decode(type: T.self), nil)
                } catch let error {
                    completionHandler(nil, error)
                }
                
            case .failure(let error): completionHandler(nil, error)
                
            }
            
        }
    }
    
    public func search(searchTerm: String, queryParams: [[String:String]]?, completionHandler: @escaping (CourseSearchResultDTO?, Error?) -> ()) {
        if searchTerm.count > 0 {
            let qp = queryParams != nil ? queryParams : [["start":"0","rows":"100","lang": Application.shared.getLanguage(), "searchType":"LO", "text": searchTerm]]
            let req = Request(endpoint: "/lo/search", pathParams: nil, queryParams: qp)
            self.networkRequest(req: req, encodeType: CourseSearchResultDTO.self) { (courseResultDTO, error) in
                completionHandler(courseResultDTO, error)
            }
        } else {
            
            completionHandler(nil, NetworkError.emptySearchTerm)
        }

        
    }
    
    func fetchAutoComplete(searchTerm: String, completionHandler: @escaping (SuggestedTermsResultDTO?, Error?)->()) {
        if searchTerm.count > 0 {
            let req = Request(
                endpoint: "/lo/autocomplete",
                pathParams: nil,
                queryParams: [["lang": Application.shared.getLanguage(), "term": searchTerm]]
            )
            self.networkRequest(req: req, encodeType: SuggestedTermsResultDTO.self) { (suggestedTerms, error) in
                completionHandler(suggestedTerms, error)
            }
        } else {
            
            completionHandler(nil, NetworkError.emptySearchTerm)
        }

    }
    
    func getCourseProviderPicture(providerId: String, completionHandler: @escaping (PictureDTO?, Error?)->()) {
        let req = Request(endpoint: "/lop/{lopId}/picture", pathParams: ["lopId": providerId])
        self.networkRequest(req: req, encodeType: PictureDTO.self) { (picture, error) in
            completionHandler(picture, error)
        }
    }
}
