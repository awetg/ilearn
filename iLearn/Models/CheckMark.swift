//
//  CheckMark.swift
//  iLearn
//
//  Created by Utsab Kharel on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

class CheckMark: UITableViewController {
    
    var data = [FacetValue]()
    
    var cellIndexPath: IndexPath
    
    var selectedLang = Set<String>()

    
    init(cell:IndexPath?) {
        self.cellIndexPath = cell!
    }
    
    
    func checkMark(){
        let cell = tableView.cellForRow(at: cellIndexPath)

        if cell!.isSelected
        {
            let value = data[cellIndexPath.row].valueId ?? ""
            self.selectedLang.insert(value)
            
            cell!.isSelected = false
            if cell!.accessoryType == UITableViewCell.AccessoryType.checkmark
                
            {
                self.selectedLang.remove(value)
                cell!.accessoryType = UITableViewCell.AccessoryType.none
            }
            else
            {
                cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
        }
    }
}
