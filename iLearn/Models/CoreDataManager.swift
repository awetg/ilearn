//
//  CoreDataManager.swift
//  iLearn
//
//  Created by iosdev on 25/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    
    //MARK: Properties
    let persistentContainer: NSPersistentContainer
    
    // shared CoreDataManager instance for all viewcontrollers
    static let shared = CoreDataManager()
    
    // Background context for creating and saving NSManagedObjects
    lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    // fetch request for facet values
    let facetValeFetchRequest: NSFetchRequest<FacetValue> = FacetValue.fetchRequest()
    
    // fetch request for course
    let courseFetchRequest: NSFetchRequest<Course> = Course.fetchRequest()
    
    // creating reusable NSpredicate objects to reduce creation of multiple NSPredicate object
    // NOTE: using LIKE operator for predicate brcause equal is not working to get specific facets, it returns all facets
    let reusablePredicateFacetField = NSPredicate(format: "facetField LIKE $newField")
    let reusablePredicateValueId = NSPredicate(format: "valueId LIKE $newValue")
    let reusablePredicateParentId = NSPredicate(format: "parentId LIKE $newParent")
    
    //MARK: init
    // initializer for passing NSPersistentContainer to CoreDataManager
    init(container: NSPersistentContainer) {
        self.persistentContainer = container
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
    }
    
    // if no NSPersistentContainer is passed this initializer will be used
    convenience init() {
        
        //use container from AppDelegate if a NSPersistentContainer is not passed
        let appDelegate: AppDelegate
        if Thread.current.isMainThread {
            appDelegate = UIApplication.shared.delegate as! AppDelegate
        } else {
            appDelegate = DispatchQueue.main.sync {
                return UIApplication.shared.delegate as! AppDelegate
            }
        }
        self.init(container: appDelegate.persistentContainer)
    }
    
    
    //MARK: DB Operation
    
    // Insert facet values to Coredata
    // remember to save inserted values, wont't persist without saving manuel
    // save is not implemented on insert function to avoid unnecessary controllerDidChangeContent calls
    // coreDataManger.save() should be called when all items are inserted to persist them to coredata
    func insertFacetValues(facetValues: [FacetValueDTO]) {
        
        facetValues.forEach {
            do {
                // check for duplicate facet value, if value does not exist new facet value object is created
                let fv = try FacetValue.getOrCreateFacetValue(valueId: $0.valueId, context: self.backgroundContext)
                fv?.facetField = $0.facetField
                fv?.valueId = $0.valueId
                fv?.valueName = $0.valueName
                fv?.count = Int32($0.count)
                fv?.parentId = $0.parentId
                fv?.childValues = Int32($0.childValues?.count ?? 0)
                if fv?.childValues ?? 0 > 0 {
                    insertFacetValues(facetValues: $0.childValues ?? [])
                }
                
            } catch let error {
                print(error)
            }
        }
    }
    
    // fetch facet values
    func fetchFacetValues(predicate: NSPredicate) -> [FacetValue] {
        facetValeFetchRequest.predicate = predicate
        let facets = try? persistentContainer.viewContext.fetch(facetValeFetchRequest)
        return facets ?? [FacetValue]()
    }
    
    
    // insert course items
    // saving course items is done by calling CoreDataManager.save()
    // save should be called when all items are insrted
    func insertCourses(courses: [CourseDTO]) {
        courses.forEach {
            do {
                
                // check for duplicate course value, if value does not exist new course value object is created
                let c = try Course.getOrCreateCourse(id: $0.id, context: self.backgroundContext)
                c?.asOngoing = $0.asOngoing ?? false
//                c?.childName = $0.childName
                c?.credits = $0.credits
                c?.educationDegree = $0.educationDegree
                c?.educationDegreeCode = $0.educationDegreeCode
                c?.educationType = $0.educationType
                c?.homeplace = $0.homeplace
                c?.id = $0.id
                c?.lopIds = $0.lopIds
                c?.lopNames = $0.lopNames
                c?.losId = $0.losId
                c?.name = $0.name
                c?.nextApplicationPeriodStarts = $0.nextApplicationPeriodStarts
                c?.parentId = $0.parentId
                c?.prerequisite = $0.prerequisite
                c?.prerequisiteCode = $0.prerequisiteCode
                c?.responsibleProvider = $0.responsibleProvider
                c?.subjects = $0.subjects
                c?.type = $0.type
                
            } catch let error {
                print(error)
            }
        }
    }
    
    func fetchCourses(predicate: NSPredicate) -> [Course] {
        courseFetchRequest.predicate = predicate
        let fetchedCourses = try? persistentContainer.viewContext.fetch(courseFetchRequest)
        return fetchedCourses ?? [Course]()
    }
    
    
    func save() {
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Coredata saving error \(error)")
            }
        }
        
    }
    
    func deleteAllCourses() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Course")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try backgroundContext.execute(deleteRequest)
            try backgroundContext.save()
            
            // clear NSFetchedResultsController cache of courses
            NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: "CourseCache")
        }
        catch let error {
            print(error)
        }
    }
    
    func deleteAllFacetValues() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FacetValue")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try backgroundContext.execute(deleteRequest)
            try backgroundContext.save()
            NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: "topicFacetCach")
        }
        catch let error {
            print(error)
        }
    }
}



// use this function to get different types of facet values
extension CoreDataManager {
    
    // get facet values by passing facetField facet object
    func fetchFacetValuesByFacetField(facetField: String) -> [FacetValue] {
        return fetchFacetValues(predicate: reusablePredicateFacetField.withSubstitutionVariables(["newField" : facetField]))
    }
    
    // get facet values by passing valueId of facet object
    func fetchFacetValuesByValueId(valueId: String) -> [FacetValue] {
        return fetchFacetValues(predicate: reusablePredicateValueId.withSubstitutionVariables(["newValue" : valueId]))
    }
    
    // get facet values by passing parentId of facet object
    func fetchChildFacetValues(parentId: String) -> [FacetValue] {
        return fetchFacetValues(predicate: reusablePredicateParentId.withSubstitutionVariables(["newParent" : parentId]))
    }
    
    // returns topic facets, topic facets have sub topic fetched by passing id of topic to fetchChildFacetValue
    func fetchTopicFacetValues() -> [FacetValue] { return fetchFacetValuesByFacetField(facetField: "theme_ffm") }
    
    // returns language facets, all languages in opintopolku
    func fetchteachingLangFacetValues() -> [FacetValue] { return fetchFacetValuesByFacetField(facetField: "teachingLangCode_ffm") }
    
    // return education type facets, education type have child values
    func fetchEducationTypeFacetValues() -> [FacetValue] { return fetchFacetValuesByFacetField(facetField: "educationType_ffm") }
    
    // prerequiste for course
    func prerequisiteFacetVAlues() -> [FacetValue] { return fetchFacetValuesByFacetField(facetField: "prerequisites") }

    //fetch time of teaching facet values
    func fetchTimeOfTeachingFacetValues() -> [FacetValue] { return fetchFacetValuesByFacetField(facetField: "timeOfTeaching_ffm") }
}
