//
//  UserData.swift
//  iLearn
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation


struct UserData: Codable {
    var email: String?
    var username: String?
   var prevEdu: String?
    var interest: UserInterest?
    var profileURL: String?
    var enrolled: [String: CourseDTO]?
    var wishlisted: [String: CourseDTO]?
}

struct UserInterest: Codable {
    var teachingLang: [String]?
    var topic: [String]?
    var subTopic: [String]?
    var educationType: [String]?
    var subEducationType: [String]?
    var timeOfTeaching: [String]?
    var formOfTeaching: [String]?
    var formOfStudy: [String]?
}
