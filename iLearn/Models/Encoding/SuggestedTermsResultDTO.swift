//
//  SuggestedTermsResultDTO.swift
//  iLearn
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public struct SuggestedTermsResultDTO: Decodable {
    var loNames: [String]?
    var keywords: [String]?
}
