//
//  PictureDTO.swift
//  iLearn
//
//  Created by iosdev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

struct PictureDTO: Decodable {
    var id: String?
    var pictureEncoded: String?
}
