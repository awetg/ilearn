//
//  CourseSearchResultDTO.swift
//  iLearn
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public struct CourseSearchResultDTO: Decodable {
    var results: [CourseDTO]
    var articleresults: [ArticleDTO]
    var providerResults: [ProviderDTO]
    var totalCount: Int
    var teachingLangFacet: FacetDTO?
    var filterFacet: FacetDTO?
    var appStatusFacet: FacetDTO?
    var edTypeFacet: FacetDTO?
    var prerequisiteFacet: FacetDTO?
    var topicFacet: FacetDTO?
    var articleContentTypeFacet: FacetDTO?
    var providerTypeFacet: FacetDTO?
    var fotFacet: FacetDTO?
    var timeOfTeachingFacet: FacetDTO?
    var formOfStudyFacet: FacetDTO?
    var applicationSystemFacet: FacetDTO?
    var siirtohakuFacet: FacetDTO?
    var lopRecommendationFilter: FacetValueDTO?
    var loCount: Int
    var articleCount: Int
    var orgCount: Int
    var educationCodeRecommendationFilter: FacetValueDTO?
    
}

struct CourseDTO: Codable {
    var id: String
    var name: String
    var lopIds: [String]?
    var lopNames: [String]?
    var prerequisite: String?
    var prerequisiteCode: String?
    var parentId: String?
    var losId: String?
    var asOngoing: Bool?
    var nextApplicationPeriodStarts: [Int]?
    var type: String?
    var credits: String?
    var educationType: String?
    var educationDegree: String?
    var educationDegreeCode: String?
    var homeplace: String?
    var childName: String?
    var subjects: [String]?
    var responsibleProvider: String?
}

struct FacetDTO: Decodable {
    var facetValues: [FacetValueDTO]
}

struct FacetValueDTO: Decodable {
    var facetField: String
    var valueId: String
    var valueName: String?
    var count: Int
    var childValues: [FacetValueDTO]?
    var parentId: String?
}

struct ProviderDTO: Decodable {
    var id: String
    var name: String
    var address: String
    var thumbnailEncoded: String?
    var providerOrg: Bool
}

struct ArticleDTO: Decodable {
    var type: String?
    var url: String?
    var title: String?
    var excerpt: String?
    var imageUrl: String?
}
