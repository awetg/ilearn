//
//  AuthenticationManager.swift
//  iLearn
//
//  Created by iosdev on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import Firebase
import CoreData

public class AuthenticationManager {
    
    static let shared = AuthenticationManager()
    
    // authenticate user
    func authenticateUser(completionHandler: @escaping (User?, Error?) -> ()) {
        if let user = Auth.auth().currentUser {
            completionHandler(user, nil)
        } else {
            completionHandler(nil, ILearnError(message: "ILearn Error: authentication errors"))
        }
    }
    
    // sign out a user from app, delete all user related data
    func signOut(completionHandler: @escaping (Error?) -> ()) {
        do{
            try Auth.auth().signOut()
            
            // delete all user related courses from coredata
            CoreDataManager.shared.deleteAllCourses()
            
            // clear NSFetchedResultsController cache of courses
            NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: "CourseCache")
            
            completionHandler(nil)
            
        }catch let error {
            completionHandler(error)
        }
    }
    
    // alias type
    typealias ThrowableCallback = () throws -> String
    
    // check if email is alread registered in Firebase for validation of email
    
    func verifyEmail(email: String, completionHandler: @escaping (ThrowableCallback) -> ()) {        Auth.auth().fetchProviders(forEmail: email) { (provider, error) in
            if provider?.count ?? 0 > 0  {
                completionHandler( {throw ILearnError(message: "Email already in use") })
            } else {
                completionHandler({ email })
            }
        }
    }
}
