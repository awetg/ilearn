//
//  FacetValue+CoreDataClass.swift
//  iLearn
//
//  Created by iosdev on 25/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


public class FacetValue: NSManagedObject {
    
    class public func getOrCreateFacetValue(valueId: String, context: NSManagedObjectContext) throws -> FacetValue? {
        
        let request: NSFetchRequest<FacetValue> = FacetValue.fetchRequest()
        request.predicate = NSPredicate(format: "valueId=%@", valueId)
        do {
            let matchingFacets = try context.fetch(request)
            if matchingFacets.count == 1 {
                return matchingFacets[0]
            } else if matchingFacets.count == 0 {
                let newFacet = FacetValue(context: context)
                return newFacet
            } else {
                print("Database inconsistency, found eqaul Facets")
                return matchingFacets[0]
            }
        }
    }

}
