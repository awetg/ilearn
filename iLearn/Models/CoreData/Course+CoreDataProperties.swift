//
//  Course+CoreDataProperties.swift
//  iLearn
//
//  Created by Viet Tran on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension Course {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Course> {
        return NSFetchRequest<Course>(entityName: "Course")
    }

    @NSManaged public var asOngoing: Bool
    @NSManaged public var credits: String?
    @NSManaged public var educationDegree: String?
    @NSManaged public var educationDegreeCode: String?
    @NSManaged public var educationType: String?
    @NSManaged public var homeplace: String?
    @NSManaged public var id: String?
    @NSManaged public var lopIds: [String]?
    @NSManaged public var lopNames: [String]?
    @NSManaged public var losId: String?
    @NSManaged public var name: String?
    @NSManaged public var nextApplicationPeriodStarts: [Int]?
    @NSManaged public var parentId: String?
    @NSManaged public var prerequisite: String?
    @NSManaged public var prerequisiteCode: String?
    @NSManaged public var responsibleProvider: String?
    @NSManaged public var subjects: [String]?
    @NSManaged public var type: String?

}
