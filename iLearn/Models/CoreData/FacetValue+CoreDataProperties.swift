//
//  FacetValue+CoreDataProperties.swift
//  iLearn
//
//  Created by iosdev on 25/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


extension FacetValue {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FacetValue> {
        return NSFetchRequest<FacetValue>(entityName: "FacetValue")
    }

    @NSManaged public var count: Int32
    @NSManaged public var facetField: String?
    @NSManaged public var parentId: String?
    @NSManaged public var valueId: String?
    @NSManaged public var valueName: String?
    @NSManaged public var childValues: Int32

}
