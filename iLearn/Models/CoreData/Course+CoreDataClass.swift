//
//  Course+CoreDataClass.swift
//  iLearn
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//
//

import Foundation
import CoreData


public class Course: NSManagedObject {

    class public func getOrCreateCourse(id: String, context: NSManagedObjectContext) throws -> Course? {
        
        let request: NSFetchRequest<Course> = Course.fetchRequest()
        request.predicate = NSPredicate(format: "id=%@", id)
        do {
            let matchingCourses = try context.fetch(request)
            if matchingCourses.count == 1 {
                return matchingCourses[0]
            } else if matchingCourses.count == 0 {
                let newCourse = Course(context: context)
                return newCourse
            } else {
                print("Database inconsistency, found eqaul Courses")
                return matchingCourses[0]
            }
        }
    }
}
