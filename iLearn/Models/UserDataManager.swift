//
//  UserDataManager.swift
//  iLearn
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import Firebase

class UserDataManager {
    
    // Firebase realtime database reference
     private let databaseReference: DatabaseReference
    
    // shared instance of UserDataManger
    static let shared = UserDataManager()
    
    // UserDefaults key for user data
    let userDataDeafultKey = "userDataDeafultKey"
    
    init(databaseReference: DatabaseReference) {
        self.databaseReference = databaseReference
    }
    
    convenience init () {
        self.init(databaseReference: Database.database().reference())
    }
    
    func loadUserData(completionHandler: @escaping (UserData?, Error?) -> ()) {
        
        // get user UID
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        // get user data from Firebase real time database
        databaseReference.child("users").child(uid).observe(.value) { snapshot in
            
            guard let value = snapshot.value as? [String: Any] else { return }
            
            // decode returned data into UserData Object
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: value, options: [])
                let userData = try JSONDecoder().decode(UserData.self, from: jsonData)
                
                // save user data into UserDefaults
                UserDefaults.standard.set(try PropertyListEncoder().encode(userData), forKey: self.userDataDeafultKey)
                
                completionHandler(userData, nil)
                
            } catch let error {
                print(error)
                completionHandler(nil, error)
            }
        }
    }
    
    func getUserData(completionHandler: @escaping (UserData?, Error?) -> ()) {
        
        do {
            // get user data from UserDefaults
            let data = UserDefaults.standard.object(forKey: self.userDataDeafultKey) as? Data
            
            // decodata data to UserData, UserData confirms to Decodable protocol
            let userData = try PropertyListDecoder().decode(UserData.self, from: data ?? Data())
            
            completionHandler(userData, nil)
            
        } catch let error {
            print(error)
            completionHandler(nil, error)
        }
    }
    
    func updateUserData(values: [String: Any], completionHandler: @escaping (Error?) -> ()) {
        AuthenticationManager.shared.authenticateUser { (user, error) in
            if error == nil {
                guard let user = user else { return }
                self.databaseReference.child("users").child(user.uid).updateChildValues(values)
                 self.loadUserData { (u,e) in}
                completionHandler(nil)
            } else {
                completionHandler(error)
            }
        }
    }
    
    
    
    
    func updateUserDataByAutoId(idName: String, values: [String: Any], completionHandler: @escaping (Error?) -> ()) {
        AuthenticationManager.shared.authenticateUser { (user, error) in
            guard let user = user else { return }
            let autoId = self.databaseReference.child("users").child(user.uid).child(idName)
            autoId.childByAutoId().setValue(values) { (error, reference) in
                if error != nil {
                    print(error!)
                } else {
                    self.loadUserData { (u,e) in}
                    print("updated data successfully")
                }
            }
        }
    }
    
    func removeEnrollOrWishlist(idName : String, idKey : String) {
        AuthenticationManager.shared.authenticateUser { (user, error) in
            guard let user = user else { return }
            let autoId = self.databaseReference.child("users").child(user.uid).child(idName).child(idKey)
            autoId.removeValue(){ (error, reference) in
                if error != nil {
                    print(error!)
                } else {
                    self.loadUserData { (u,e) in}
                    print("remove data successfully")
                }
            }
        }
    }
    
    func uploadUserProfileImage(image: UIImage, completionHandler: @escaping (URL?, Error?) -> ()) {
        let storageRef = Storage.storage().reference().child("myImage.png")
        let imgData = image.pngData()
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        storageRef.putData(imgData!, metadata: metaData){ (metaData, error) in
            if error == nil {
                storageRef.downloadURL{ (url, error) in
                    completionHandler(url, error)
                }
            }else{
                completionHandler(nil, error)
            }
        }
    }
    
    func loadUserRelatedCourses() {
        self.loadUserData { (userData, error) in
            if error == nil {
                
                // API request to get courses(LO) with query params
                var req = [["lang": Application.shared.getLanguage(), "searchType":"LO","text":"*"]]
                
                // if user have learning preference download course usign those paramaters
                if userData?.interest != nil {
                    // add topic of education as filter if exist
                    userData?.interest?.topic?.forEach {
                        req.append(["facetFilters": FacetFilterFields.topic + ":" + $0])
                    }
                    
                    // add teaching language filter if exist
                    userData?.interest?.teachingLang?.forEach {
                        req.append(["facetFilters": FacetFilterFields.teachingLang + ":" + $0])
                    }
                    
                    // add form of study filter if exist
                    userData?.interest?.formOfStudy?.forEach {
                        req.append(["facetFilters": FacetFilterFields.formOfStudy + ":" + $0])
                    }
                    
                    // add form of teaching filter if exist
                    userData?.interest?.formOfTeaching?.forEach {
                        req.append(["facetFilters": FacetFilterFields.formOfTeaching + ":" + $0])
                    }
                    
                    // add education type filter
                    userData?.interest?.educationType?.forEach {
                        req.append(["facetFilters": FacetFilterFields.educationType + ":" + $0])
                    }
                } else {
                    // if user don't have learning preference set use generic parametes
                    let genericParameters =
                        [
                            ["facetFilters":"teachingLangCode_ffm:EN"],
                            ["facetFilters":"timeOfTeaching_ffm:opetusaikakk_1"],
                            ["facetFilters":"formOfStudy_ffm:opetusmuotokk_1"],
                            ["facetFilters":"prerequisites:eipk"]
                        ]

                    req = req + genericParameters
                }
                

                
                //search course with this query parameters from Opintopolku API
                // starting number of courses requested at first
                var rows = [["start":"0","rows":"100"]]
                CourseDataManager().search(searchTerm: "*", queryParams: req + rows) { (searchResults, error) in

                    // insert all returned courses to core data
                    CoreDataManager.shared.insertCourses(courses: searchResults?.results ?? [CourseDTO]())
                    CoreDataManager.shared.save()

                    let numberOfCourses = searchResults?.loCount ?? 0

                    // if there are more than 100 course items for this query, fetch the remaining
                    // fetch wil be done 100 items per request
                    if numberOfCourses > 100 {

                        // concurrent queue to fetch remaining courses
                        let concurrentQueue = DispatchQueue(
                            label: Bundle.main.bundleIdentifier! + ".concurrentQueue", attributes: .concurrent
                        )

                        let quotient = numberOfCourses / 100
                        let remainder = numberOfCourses % 100

                        // quotient will not be less 0 since number of courses is more than 100
                        (1...quotient).forEach { i in
                            
                            // update request with
                            rows = [["start": String(i * 100), "rows": String(i == quotient ? i * 100 + remainder : i * 100 + 100)]]
                            
                            concurrentQueue.async {
                                CourseDataManager().search(searchTerm: "*", queryParams: req + rows) { (searchResponse, error) in
                                    CoreDataManager.shared.insertCourses(courses: searchResponse?.results ?? [CourseDTO]())
                                    CoreDataManager.shared.save()
                                }
                            }
                        }

                    }
            }
            } else {
                print("Error: error loading user related courses \(String(describing: error))")
            }
        }
    }
    
    func reloadUserRelatedCourses() {
        // delete all user related courses from coredata
        CoreDataManager.shared.deleteAllCourses()
        
        // load user data again
        self.loadUserRelatedCourses()
    }
}


