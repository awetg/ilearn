//
//  Common.swift
//  iLearn
//
//  Created by iosdev on 21/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case post = "POST"
    case put = "PUT"
    case get = "GET"
    case delete = "DELETE"
    case patch = "PATCH"
}

public enum NetworkError: String, Error, CustomStringConvertible {
    
    case badInput = "ERROR_iLearn: Bad Input"
    
    case invalidURL = "ERROR_iLearn: Invalid Url, can't create url with input"
    
    case pathParams = "ERROR_iLearn: None url encodable or nil path parameter passed"
    
    case badResponse = "ERROR_iLearn: Network response error. Hint: check http response for possible errors"
    
    case networkMock = "ERROR_iLearn: network mock classes error"
    
    case emptySearchTerm = "ERROR_iLearn: Empty search term recieved"
    
    public var description: String {
        return self.rawValue
    }
}


// below are directly taken from this githaub gist
/* https://gist.github.com/CassiusPacheco/b23aa85e2936f6f514643faceb3e2fec#file-di_and_tests_part2_2-swift */

public protocol URLSessionProtocol: class {
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskProtocol
}

public protocol URLSessionDataTaskProtocol {
    
    func resume()
}

extension URLSessionDataTask: URLSessionDataTaskProtocol { }


extension URLSession: URLSessionProtocol {
    
    public func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskProtocol {
        return ((dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask) as URLSessionDataTaskProtocol)
    }
}
