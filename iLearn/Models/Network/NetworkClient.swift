//
//  NetworkClient.swift
//  iLearn
//
//  Created by iosdev on 20/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation


// Extend NetworkClientProtocol to create your own Network client
//
public protocol NetworkClientProtocol {
    
    var baseUrl: String { get }
    
    func execute(_ request: RequestProtocol, completionHandler: @escaping (ResponseProtocol) -> ())
}

// default implementaion
extension NetworkClientProtocol {


}

public class NetworkClient: NetworkClientProtocol {
    
    public var baseUrl: String
    
    private var urlSession: URLSessionProtocol
    
    public init(baseUrl: String =  "https://opintopolku.fi", urlSession: URLSessionProtocol = URLSession.shared) {
        self.baseUrl = baseUrl
        self.urlSession = urlSession
    }
    
    public func execute(_ request: RequestProtocol, completionHandler: @escaping (ResponseProtocol) -> ()) {
        do {
            let task = try urlSession.dataTask(with: request.urlRequest(in: self)) { data, urlResponse, error in
                
                guard
                    let httpResponse = urlResponse as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode),
                    let data2 = data,
                    let _ = try? JSONSerialization.jsonObject(with: data2, options: [.allowFragments]) as? [String: Any]
                    else {
                        // return a fauilure type of response
                        let result = Response.Result<Data>.failure(error ?? NetworkError.badResponse)
                        let httpRes = urlResponse as? HTTPURLResponse
                        completionHandler(Response(httpResponse:httpRes, request: request, data: data, result: result))
                        return
                }
                
                // return a success response with data and httpResponse
                let result =  Response.Result<Data>.success(data2)
                completionHandler(Response(httpResponse:httpResponse, request: request, data: data2, result: result))
                
            }
            task.resume()
            
        } catch let error {
            // url initialization error, return failure response
            let result = Response.Result<Data>.failure(error)
            completionHandler(Response(httpResponse: nil, request: request, data: nil, result: result))
        }
        
    }
    
}
