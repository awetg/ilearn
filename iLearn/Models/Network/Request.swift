//
//  Request.swift
//  iLearn
//
//  Created by iosdev on 21/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

// Extend RequestProtocol to create your own type of Request
//
public protocol RequestProtocol {
    
    // endpoint of the request url
    // if endpoint have replaceable path parameters they should be set as e.e "/course/category/{category}/id/{art_id}"
    var endpoint: String { get set }
    
    var method: HTTPMethod? { get set }
    
    // url paramaters, this wil appended after encoding e.g ?start=0&rows=100
    // NOTE: query parameter data structure is an array of dictionary because Opintoplku API use query paramater keys
    // multiple times e.g ?facetFilter=topic_ffm:topicname&faceFilter=topic_ffm:topicname
    var queryParams: [[String: String]]? { get set }
    
    //path parameter of url in an array ordered by occurance e.e "/course/category/{category}/id/{art_id}"
    // if no replaceable path parameters are set on baseUrl or baseUrl endpoint, this path parameters will not be set
    var pathParams:  [String: Any?]? { get set }
    
    var headers: [String: String]? { get set }
    
    // return full URL of request
    func url(in networkClient: NetworkClientProtocol) throws -> URL
    
    // retursn full URLRequest of this request object
    func urlRequest(in networkClient: NetworkClientProtocol) throws -> URLRequest
}

// default implementation of protocol functions
public extension RequestProtocol {
    func url(in networkClient: NetworkClientProtocol) throws -> URL {
        
        // encode path parameters e.g "/course/{category}/id/{course_id}"
        var stringUrl = networkClient.baseUrl + self.endpoint
        
        // add replace path params if present
        stringUrl = pathParams?.isEmpty ?? true ? stringUrl : stringUrl.fillWithPathParams(withValues: pathParams)
        
        // add query params if not empty
        stringUrl = queryParams?.isEmpty ?? true ? stringUrl : try queryParams?.encodeQueryParams(base: stringUrl) ?? stringUrl
        
        // create url with full built string url
        guard let url = URL(string: stringUrl) else {
            throw NetworkError.invalidURL
        }
        return url
    }
    
    func urlRequest(in networkClient: NetworkClientProtocol) throws -> URLRequest {
        
        let url = try self.url(in: networkClient)
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = (self.method ?? .get).rawValue
        if (self.headers != nil) {
            urlRequest.allHTTPHeaderFields = self.headers
        }
        return urlRequest
    }
}

class Request: RequestProtocol {
    
    var endpoint: String
    
    var method: HTTPMethod?
    
    var headers: [String: String]?
    
    var queryParams: [[String: String]]?
    
    var pathParams: [String : Any?]?
    
    public init(endpoint: String?, pathParams: [String : Any?]?, queryParams: [[String: String]]? = nil) {
        self.method = .get
        self.endpoint = endpoint ?? ""
        self.pathParams = pathParams
        self.queryParams = queryParams
    }
    
    public init(
       endpoint: String?, method: HTTPMethod?, pathParams: [String : Any?]?, queryParams: [[String: String]]? = nil, headers:[String: String]?
        ) {
        self.method = method ?? .get
        self.endpoint = endpoint ?? ""
        self.pathParams = pathParams
        self.queryParams = queryParams
        self.headers = headers
    }
}
