//
//  Response.swift
//  iLearn
//
//  Created by iosdev on 21/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

// Response of a network operation
//
public protocol ResponseProtocol {
    // type of response, failure of success
    var result: Response.Result<Data> { get }
    
    // data returned from network call if successful
    var data: Data? { get }
    
    var httpResponse: HTTPURLResponse? { get }
    
    // Original network request that initiated this response
    var request: RequestProtocol { get }
    
    // decode data to decadable type
    func decode<T: Decodable>(type: T.Type) throws -> T
}

public extension ResponseProtocol {
    func decode<T: Decodable>(type:  T.Type) throws -> T {
        do {
            return try JSONDecoder().decode(type, from: self.data ?? Data())
        } catch let error {
            throw error
        }
    }
    
}

public class Response: ResponseProtocol {
    
    public var result: Response.Result<Data>
    
    public var data: Data?
    
    public var httpResponse: HTTPURLResponse?
    
    public var request: RequestProtocol

    public enum Result<T> {
        case success(T)
        case failure(Error)
    }
    
    public init(httpResponse: HTTPURLResponse?, request: RequestProtocol, data: Data?, result: Result<Data>) {
        self.httpResponse = httpResponse
        self.request = request
        self.data = data
        self.result = result
        
    }
}
