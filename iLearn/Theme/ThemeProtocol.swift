//
//  ThemeProtocol.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 02/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

protocol ThemeProtocol {
    var mainFontName: String { get }
    var textColor: UIColor { get }
    var accent: UIColor { get }
    var background: UIColor { get }
    var tint: UIColor { get }
    var boxColor: UIColor { get }
    var buttonColor: UIColor { get }


}
