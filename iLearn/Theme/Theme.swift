//
//  Theme.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 02/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

class Theme {
    static var current: ThemeProtocol = LightTheme()
}
