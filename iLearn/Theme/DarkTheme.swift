//
//  DarkTheme.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 02/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

class DarkTheme: ThemeProtocol {
    var mainFontName: String = "Helvetica Neue Light"
    
    var textColor: UIColor = UIColor.white

    var accent: UIColor = UIColor(named: "Background")!
    
    var background: UIColor = UIColor(named: "Accent")!
    
    var tint: UIColor = UIColor(named: "Tint")!
    
    var boxColor: UIColor = UIColor.lightGray
    
    var buttonColor: UIColor = UIColor(named: "Tint")!

    
}
