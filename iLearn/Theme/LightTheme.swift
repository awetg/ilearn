//
//  LightTheme.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 02/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

class LightTheme: ThemeProtocol {
    var mainFontName: String = "Helvetica Neue Light"
    
    var textColor: UIColor = UIColor.black
    
    var accent: UIColor = UIColor(named: "Accent")!
    
    var background: UIColor = UIColor(named: "Background")!
    
    var tint: UIColor = UIColor(named: "Tint")!
    
    var boxColor: UIColor = UIColor.white
    
    var buttonColor: UIColor = UIColor(named: "Default")!

    
    var navigationBarAppearance = UINavigationBar.appearance()
    

    func colors () {
    navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
}
    
}
