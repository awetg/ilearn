//
//  SwiftClassExtention.swift
//  iLearn
//
//  Created by iosdev on 20/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
import UIKit

// --------- SWIFT Codable extentsion -----------------
// MARK: Codable

// decode a class that confirms to Decodable
extension Decodable {
    static func decode(with decoder: JSONDecoder = JSONDecoder(), from data: Data) throws -> Self {
        return try decoder.decode(Self.self, from: data)
    }
}

// encode a class that confirms to Encodable
extension Encodable {
    func encode(with encoder: JSONEncoder = JSONEncoder()) throws -> Data {
        return try encoder.encode(self)
    }
}


// ------------ SWIFT Dictionary extension ---------------
// MARK: Dictionary

extension Dictionary where Key == String, Value == String {
    
    // set query parameters to url e.g ?start=0&rows=100
    public func asQueryParams() -> [URLQueryItem] {
        // nothing to encode
        guard self.count > 0 else { return [URLQueryItem]() }
        return self.compactMap { (key,value) in
            return URLQueryItem(name: key, value: String(describing: value))
        }
    }
    
}


// ---------- SWIFT String extension -------------
// MARK: String

public extension String {
    
    /// Encode a dictionary as url encoded string for path parameters e.g "/course/{category}/id/{course_id}"
    func fillWithPathParams(withValues dict: [String: Any?]?) -> String {
        guard let data = dict else {
            return self
        }
        var finalString = self
        data.forEach { arg in
            if let unwrappedValue = arg.value {                finalString = finalString.replacingOccurrences(of: "{\(arg.key)}", with: String(describing: unwrappedValue))
            }
        }
        return finalString
    }
    
    func localizableString(loc: String) -> String {
        print("current lang", loc)
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        let localized =  NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        print(localized)
        return localized
    }
}

extension Array where Element == [String: String] {
    
    // set query parameters to url e.g ?start=0&rows=100
    public func encodeQueryParams(base: String = "") throws -> String {
        guard self.count > 0 else { return base }
        var items: [URLQueryItem] = []
        self.forEach {
            items.append(contentsOf: $0.asQueryParams())
        }
        var urlComponents = URLComponents(string: base)!
        urlComponents.queryItems = urlComponents.queryItems ?? [] + items
        guard let encodedString = urlComponents.url else {
            throw NetworkError.pathParams
        }
        return encodedString.absoluteString
    }
}

// ---------- SWIFT UIImageView extension -------------
// MARK: UIImageView

// entend imageview to download images from url
extension UIImageView {
    func downloadImage(url: String) {
        guard let readyUrl = URL(string: url) else { return }
        let urlRequest = URLRequest(url: readyUrl)
        let task: URLSessionDataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print ("Image download error\(error)")
            }
            DispatchQueue.main.async {
                guard let data2 = data  else { return }
                self.image = UIImage(data: data2)
            }
        }
        task.resume()
    }
    
    func getProviderPicture(providerId: String, altUrl: String) {
        CourseDataManager().getCourseProviderPicture(providerId: providerId) { (picture, error) in
            if error != nil {
                self.downloadImage(url: altUrl)
            } else {
                DispatchQueue.main.async {
                    guard let decodedData = Data(base64Encoded: picture?.pictureEncoded ?? "") else { return }
                    self.image = UIImage(data: decodedData)
                }
            }
        }
    }
}



// ---------- SWIFT UIColor extension -------------
// MARK: UIColor

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}



// ---------- SWIFT UIViewController extension -------------
// MARK: UIViewController

fileprivate var aView: UIView?

extension UIViewController {
    
    func showSpinner(timeInterval: TimeInterval = 5.0) {
        aView = UIView(frame: self.view.bounds)
        aView?.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView(style: .whiteLarge)
        ai.center = aView!.center
        ai.startAnimating()
        aView?.addSubview(ai)
        self.view.addSubview(aView!)
        
        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false) { (t) in
            self.removeSpinner()
        }
        
    }
    
    func removeSpinner() {
        aView?.removeFromSuperview()
        aView = nil
    }
    
    func showAlert(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
}


// ---------- SWIFT UITextField extension -------------
// MARK: UITextField

extension UITextField {
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validate(value: self.text!)
    }
}
