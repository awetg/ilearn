//
//  ViewController.swift
//  iLearn
//
//  Created by iosdev on 19/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import CoreData

class MainTabBarVC: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // authenticate user, show authentication interface if not logged in
        self.athenticateUser()
        
        // download user related courses
        UserDataManager.shared.loadUserRelatedCourses()
    }
    
    func athenticateUser() {
        AuthenticationManager.shared.authenticateUser { (user, error) in
            if error != nil {
                DispatchQueue.main.async {
                    let logInVC = UIStoryboard(name: "Auth",bundle: nil).instantiateViewController(withIdentifier: "SignInView") as! LogInVC
                    self.navigationController?.pushViewController(logInVC, animated: true)//(logInVC, animated: false)
                }
            }
        }
    }
    
}
