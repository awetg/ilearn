//
//  EnrollViewController.swift
//  Blops
//
//  Created by Viet Tran on 01/05/2019.
//  Copyright © 2019 Viet Tran. All rights reserved.
//

import UIKit
import Firebase

class EnrollViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var enrollTableView: UITableView!
    
    var courseArray = [CourseDTO]()
    
    override func viewWillAppear(_ animated: Bool) {
        getEnrollFromFirebase()
        enrollTableView.reloadData()
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        enrollTableView.delegate = self
        enrollTableView.dataSource = self
        
        getEnrollFromFirebase()
        enrollTableView.reloadData()
        //print("AAAA")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = enrollTableView.dequeueReusableCell(withIdentifier: "searchCourseTableCell", for: indexPath) as! CourseTableCell
        cell.setItem(course: courseArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let course = courseArray[indexPath.row]
        let courseDetail = CourseDetail(course: course)
        let vc = CourseDetailVC.createCourseDetailVC(course: courseDetail)
        self.navigationController?.show(vc, sender: self)
    }
    
    func getEnrollFromFirebase() {
        courseArray.removeAll()
        UserDataManager.shared.getUserData { (userData, error) in
            userData?.enrolled?.forEach { (k,v) in
                print(v)
                self.courseArray.append(v)
            }
            self.enrollTableView.reloadData()
        }
    }
    
    


}
