//
//  DetailController.swift
//  Blops
//
//  Created by Viet Tran on 01/05/2019.
//  Copyright © 2019 Viet Tran. All rights reserved.
//

import UIKit
import Firebase

class CourseDetailVC: UIViewController {
    
    var course: CourseDetail?

//    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var prerequisite: UILabel!
    @IBOutlet weak var credit: UILabel!
    @IBOutlet weak var educationDegree: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var homeplace: UILabel!
    
    @IBOutlet weak var enrollButton: UIButton!
    @IBOutlet weak var wishlistButton: UIButton!
    
    var wishlistId = ""
    var enrolledId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        image.downloadImage(url: "https://picsum.photos/130/120")

        name.text = "Course name: \(course?.name ?? "no name")"
        prerequisite.text = "Prerequisite: \(course?.prerequisite ?? "")"
        credit.text = "Credits: \(course?.credits ?? "")"
        educationDegree.text = "Education Degree: \(course?.educationDegree ?? "")"
        type.text = "Type: \(course?.type ?? "")"
        homeplace.text = "Home place: \(course?.homeplace ?? "")"
        self.isEnrolled()
        self.isWishlisted()
        
        if !enrolledId.isEmpty {
            enrollButton.setTitle("Remove Enrolled", for: .normal)
            enrollButton.backgroundColor = UIColor.red
            wishlistButton.isHidden = true
        } else {
            if !wishlistId.isEmpty {
                wishlistButton.setTitle("Remove Wishlisted", for: .normal)
                wishlistButton.backgroundColor = UIColor.red
            }
        }
        
    }
    

    @IBAction func enroll(_ sender: Any) {
        if enrolledId.isEmpty {
            addData(idName: "enrolled")
            if !wishlistId.isEmpty {
                UserDataManager.shared.removeEnrollOrWishlist(idName: "wishlisted", idKey: wishlistId)
            }
        } else {
            removeData(idName: "enrolled", idKey: enrolledId)
        }
        
    }
    
    @IBAction func wishlist(_ sender: Any) {
        if wishlistId.isEmpty {
               addData(idName: "wishlisted")
        } else {
            removeData(idName: "wishlisted", idKey: wishlistId)
        }

    }
    
    // init CourseListVC using storyboard
    static func createCourseDetailVC(course: CourseDetail) -> CourseDetailVC {
        let vc = UIStoryboard(name: "Course",bundle: nil).instantiateViewController(withIdentifier: "CourseDetailVC") as! CourseDetailVC
        vc.course = course
        return vc
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getCourseData() -> [String: Any?] {
        let data: [String: Any?] = [
            
            "asOngoing": course?.asOngoing,
            "credits": course?.credits,
            "educationDegree": course?.educationDegree,
            "educationDegreeCode": course?.educationDegreeCode,
            "educationType": course?.educationType,
            "homeplace": course?.homeplace,
            "id": course?.id,
            "lopIds": course?.lopIds,
            "lopNames": course?.lopNames,
            "losId": course?.losId,
            "name": course?.name,
            "nextApplicationPeriodStarts": course?.nextApplicationPeriodStarts,
            "parentId": course?.parentId,
            "prerequisite": course?.prerequisite,
            "prerequisiteCode": course?.prerequisiteCode,
            "responsibleProvider": course?.responsibleProvider,
            "subjects": course?.subjects,
            "type": course?.type
            
        ]
        return data
    }
    
    func isWishlisted() {
        UserDataManager.shared.getUserData { (userData, error) in
            guard let wishlisted = userData?.wishlisted else { return }
            for wl in wishlisted {
                if self.course?.id == wl.value.id {
                    self.wishlistId = wl.key
                }
            }
            //self.enrollButton.backgroundColor = self.wishlistId.isEmpty ? UIColor.blue : UIColor.red
        }
    }
    
    func isEnrolled() {
        UserDataManager.shared.getUserData { (userData, error) in
            guard let enrolled = userData?.enrolled else { return }
            for en in enrolled {
                if self.course?.id == en.value.id {
                    self.enrolledId = en.key
                }
            }
            
            //self.wishlistButton.isHidden = self.wishlistId.isEmpty ? false : true
        }
    }
    
    func addData(idName : String) {
        UserDataManager.shared.updateUserDataByAutoId(idName: idName, values: getCourseData() as [String : Any]) { error in
            if error != nil {
                print(error!)
            }
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func removeData(idName: String, idKey : String) {
        UserDataManager.shared.removeEnrollOrWishlist(idName: idName, idKey: idKey)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}
