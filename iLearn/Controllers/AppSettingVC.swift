//
//  AccountSettingsVC.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class AppSettingsVC: UIViewController {

    
    @IBOutlet weak var topview: UIView!
    
    @IBOutlet weak var bottomView: UIView!
    @IBAction func closeVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switchBtn.isOn = UserDefaults.standard.bool(forKey: "DarkTheme")
        switchBtn.addTarget(self, action: #selector(themeChange(_:)), for: .touchUpInside)
        self.setupView()
        self.seg.selectedSegmentIndex = Application.shared.getLanguage() == "en" ? 0 : 1
        applyTheme()
    }
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var userSettings: UINavigationItem!
    @IBOutlet weak var basicSettings: UILabel!
    @IBOutlet weak var lightThemeLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var othersLabel: UILabel!
    @IBOutlet weak var feedbackLabel: UIButton!
    @IBOutlet weak var legalBtn: UIButton!
    
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    
    
    
    
    
    @IBOutlet weak var seg: UISegmentedControl!
    @IBAction func languageSegmentControl(_ sender: UISegmentedControl) {
        segmentSwitch()
    }
    @IBAction func themeChange(_ sender: UISwitch) {
        
        Theme.current = sender.isOn ? DarkTheme() : LightTheme()
        
        UserDefaults.standard.set(sender.isOn, forKey: "DarkTheme")
        
        applyTheme()
    }
    
    
    func segmentSwitch() {
        if seg.selectedSegmentIndex == 0 {  // English
            
            // if application lang was in finnish reload all data
            let current = Application.shared.getLanguage()
            Application.shared.setLanguage(lang: "en")
            if current != "en" {
                Application.shared.reloadFacetValues()
                UserDataManager.shared.reloadUserRelatedCourses()
            }
            self.setupView()
        }else { // finnish
            
            // if application lang was english reload all data
            let current = Application.shared.getLanguage()
            Application.shared.setLanguage(lang: "fi")
            if current != "fi" {
                Application.shared.reloadFacetValues()
                UserDataManager.shared.reloadUserRelatedCourses()
            }
            self.setupView()
        }
    }
    
    
    fileprivate func applyTheme() {
        view.backgroundColor = Theme.current.background
        lightThemeLabel.textColor = Theme.current.textColor
        doneBtn.tintColor = Theme.current.buttonColor
        basicSettings.textColor = Theme.current.textColor
        languageLabel.textColor = Theme.current.textColor
        othersLabel.textColor = Theme.current.textColor
        feedbackLabel.tintColor = Theme.current.buttonColor
        legalBtn.tintColor = Theme.current.buttonColor
        topview.backgroundColor = Theme.current.boxColor
        bottomView.backgroundColor = Theme.current.boxColor
    }
    
    func setupView() {
        languageLabel.text = "LanKey".localizableString(loc: Application.shared.getLanguage())
        doneBtn.title = "DoneKey".localizableString(loc: Application.shared.getLanguage())
        lightThemeLabel.text = "DarkModeKey".localizableString(loc: Application.shared.getLanguage())
        basicSettings.text = "BasicSettingsKey".localizableString(loc: Application.shared.getLanguage())
        othersLabel.text = "OthersKey".localizableString(loc: Application.shared.getLanguage())
        navigationItem.title = "UserSettingsKey".localizableString(loc: Application.shared.getLanguage())
        feedbackLabel.setTitle("FeedbackKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
        legalBtn.setTitle("LegalKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
        
    }
}

