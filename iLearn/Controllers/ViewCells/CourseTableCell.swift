//
//  CourseTableCell.swift
//  iLearn
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class CourseTableCell: UITableViewCell {
    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var courseName: UILabel!
    @IBOutlet weak var courseCredits: UILabel!
    @IBOutlet weak var homePlace: UILabel!
    
    func setItem(course: CourseDTO?) {
        courseName.text = course?.name
        courseCredits.text = course?.credits
        homePlace.text = course?.homeplace
        courseImg.getProviderPicture(providerId: course?.lopIds?.first ?? "", altUrl: "https://picsum.photos/104/104?blur=2")
    }
}
