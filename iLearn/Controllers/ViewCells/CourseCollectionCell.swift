//
//  CourseCollectionCell.swift
//  iLearn
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class CourseCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var courseImg: UIImageView!
    @IBOutlet weak var courseName: UILabel!
    @IBOutlet weak var courseCredit: UILabel!
    @IBOutlet weak var homePlace: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCourse(course: Course) {
        courseName.text = course.name
        courseCredit.text = course.credits
        homePlace.text = course.homeplace
        courseImg.getProviderPicture(providerId: course.lopIds?.first ?? "", altUrl: "https://picsum.photos/166/70?blur=3")
        
    }
    
    func setCourseDTO(course: CourseDTO) {
        courseName.text = course.name
        courseCredit.text = course.credits
        homePlace.text = course.homeplace
        courseImg.getProviderPicture(providerId: course.lopIds?.first ?? "", altUrl: "https://picsum.photos/166/70?blur=3")
    }
}
