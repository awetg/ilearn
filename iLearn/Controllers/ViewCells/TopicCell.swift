//
//  TopicCell.swift
//  iLearn
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class TopicCell: UITableViewCell {

    @IBOutlet weak var topicIcon: UIImageView!
    @IBOutlet weak var topicName: UILabel!
    
    func setCell(topicFacet: FacetValue) {
        self.topicIcon.image = topicIcons[topicFacet.valueId ?? ""]
        self.topicName.text = topicFacet.valueName
    }
}
