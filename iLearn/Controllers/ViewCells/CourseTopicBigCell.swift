//
//  CourseTopicBigCell.swift
//  iLearn
//
//  Created by Utsab Kharel on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class CourseTopicBigCell: UITableViewCell {
    
    @IBOutlet weak var courseTopicImg: UIImageView!
    
    @IBOutlet weak var courseTopicName: UILabel!
    
    func setTopic(courseFacetValues: FacetValue) {
        courseTopicName.text = courseFacetValues.valueName ?? "Course topic not found"
        courseTopicImg.downloadImage(url: "https://picsum.photos/130/120")
    }

}
