//
//  RecommendationCell.swift
//  iLearn
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class RecommendationCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewAllLabel: UILabel!
    
    weak var delegateVC: UIViewController?
    
    var courses: [Course] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
    
    func setItem(recommendation: Recommendation) {
        self.title.text = recommendation.title
        self.courses = recommendation.courses
        self.viewAllLabel.alpha = courses.count > 5 ? 0.9 : 0.3
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return courses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "courseCollectionCell", for: indexPath) as! CourseCollectionCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.cornerRadius = 3
        cell.setCourse(course: courses[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let course = courses[indexPath.row]
        let courseDetail = CourseDetail(course: course)
        let vc = CourseDetailVC.createCourseDetailVC(course: courseDetail)
        self.delegateVC?.show(vc, sender: self.delegateVC)
    }

}
