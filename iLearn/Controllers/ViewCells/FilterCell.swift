//
//  FilterCell.swift
//  iLearn
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {
    @IBOutlet weak var filterOption: UILabel!
    @IBOutlet weak var checkedImg: UIImageView!
}
