//
//  SliderCollectionCell.swift
//  iLearn
//
//  Created by iosdev on 08/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class SliderCollectionCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var sliderTitle: UILabel!
    
    func setItem(slider: SliderItem) {
        image.image = slider.image
        sliderTitle.text = slider.title
    }
}
