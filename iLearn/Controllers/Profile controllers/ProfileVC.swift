//
//  ProfileVC.swift
//  iLearn
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import Firebase

class ProfileVC: UIViewController {
    
    let signInViewController = UIStoryboard(name: "Auth",bundle: nil).instantiateViewController(withIdentifier: "SignInView") as! LogInVC
    
    @IBOutlet var popUpView: UIView!
    
    @IBAction func openPopUp(_ sender: Any) {
        animateIn()
        
    }
    @IBAction func dismissPopUp(_ sender: Any) {
        self.popUpView.removeFromSuperview()
    }
    @IBAction func aboutBtn(_ sender: Any) {
         self.popUpView.removeFromSuperview()
    }
    @IBAction func toEditProfile(_ sender: Any) {
         self.popUpView.removeFromSuperview()
    }
    @IBAction func toSettingsBtn(_ sender: Any) {
         self.popUpView.removeFromSuperview()
    }
    
    @IBAction func signOutbtn(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to signout ?", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "SignOut", style: .destructive, handler: { (_) in
            self.signOut()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 10
        popUpView.layer.masksToBounds = true
        self.title = "Profile"
    }
    
    func animateIn() {
        self.view.addSubview(popUpView)
        popUpView.center = self.view.center
        popUpView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        popUpView.alpha = 0
        
        UIView.animate(withDuration: 0.5){
            self.popUpView.alpha = 1
            self.popUpView.transform = CGAffineTransform.identity
            
        }
    }
    
    func animateOut() {
        UIView.animate(withDuration: 0.3, animations: {
            self.popUpView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.popUpView.alpha = 0
            
        }) { (success: Bool) in
            self.popUpView.removeFromSuperview()
        }
    }
    
    func signOut(){
        AuthenticationManager.shared.signOut { error in
            if error == nil {
                self.present(self.signInViewController, animated: true, completion: nil)
            } else {
                print(error ?? "Signout error")
            }
        }
    }
    
    @IBAction func unwindToProfile(_ sender: UIStoryboardSegue) {}

}
