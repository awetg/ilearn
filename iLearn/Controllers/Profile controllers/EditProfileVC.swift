//
//  EditProfileVC.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 25/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import Firebase

class EditProfileVC: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var previousEducationTable: UITableView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmpassTextField: UITextField!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var design_button: UIButton!

    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    @IBOutlet weak var editProfile: UINavigationItem!
    
    @IBOutlet weak var learnBtn: UIButton!
    // user auth data and user profile data
    var userData: UserData?
    var user: User?
    
    // new input for authentication data
    var newPassword = ""
    var newEmail = ""
    var newUsername = ""
    var confirmPassword = ""
    
    // prerequisites as previous education
    var selectedPrevEduId = ""
    let previousEducation: [FacetValue] = CoreDataManager.shared.fetchFacetValuesByFacetField(facetField: "prerequisites")
    
    // user interset or learning preference of user
    // used to recommend courses to user, saved as user profile data in firebase and can be updated
    var learningPreference: [LearningPreference] = [LearningPreference]()
    
    var learningPreferenceChanged = false
    
    var profileimageChanged = false
    
    let imagePicker = UIImagePickerController()
    var navigationBarAppearance = UINavigationBar.appearance()
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    // MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup data and views
        self.setLanView()
        self.loadUserData()
        self.setUpViews()
        self.previousEducationTable.dataSource = self
        self.previousEducationTable.delegate = self
        
        self.userNameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.confirmpassTextField.delegate = self
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        colors()
        
    }
    
    
    @IBAction func previousEducationToggle(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.previousEducationTable.isHidden = !self.previousEducationTable.isHidden
        }
    }
    
    @IBAction func openLearningPreference(_ sender: Any) {
        performSegue(withIdentifier: "learningPreferenceSegue", sender: self)
    }
    
    @IBAction func closeEdit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? LearningPreferenceVC {
            vc.prefrences = self.learningPreference
            
            // closure delegate for passing selected learning preference
            // using weak self reference to avoid reference cycles
            vc.updateLearningPreference = { [weak self] (preferences, isChanged) in
                self?.learningPreference = preferences
                self?.learningPreferenceChanged = isChanged
            }
        }
    }

    
    @IBAction func updateBtn(_ sender: Any) {
        
        // using DispatchGroup to dismis when all task complete
        let group = DispatchGroup()
        
        group.enter()
        self.updateEmail { group.leave()}
        
        group.enter()
        self.updatePassword { group.leave()}
        
        group.enter()
        self.updateProfileImage { group.leave()}
        
        group.enter()
        self.updateProfileData { group.leave()}

        self.showSpinner()
        
        group.notify(queue: .main) {
            self.removeSpinner()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setLanView() {
        userNameTextField.text = "UsernameKey".localizableString(loc: Application.shared.getLanguage())
        cancelBtn.title = "CancelKey".localizableString(loc: Application.shared.getLanguage())
        emailTextField.text = "EmailKey".localizableString(loc: Application.shared.getLanguage())
        passwordTextField.text = "PasswordKey".localizableString(loc: Application.shared.getLanguage())
        confirmpassTextField.text = "ConfirmKey".localizableString(loc: Application.shared.getLanguage())
        navigationItem.title = "EditProfileKey".localizableString(loc: Application.shared.getLanguage())
        //       previousEducationTable.textInputMode = "EditProfileKey".localizableString(loc: Application.shared.getLanguage())
        learnBtn.setTitle("LearningKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
        design_button.setTitle("UpdateKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
    }
    
}

extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    // open gallery
    
    @objc func openGallery(tapGesture: UITapGestureRecognizer){
        self.setupImagePicker()
    }
    
    func setupImagePicker(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.delegate = self
            imagePicker.isEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profilePicture.image = image
        self.profileimageChanged = true
        self.dismiss(animated: true, completion: nil)
    }
}

extension EditProfileVC: UITextFieldDelegate {
    
    public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        switch textField {
        case emailTextField:
            do {
                newEmail = try emailTextField.validatedText(validationType: .email)
            } catch let error {
                self.showAlert(title: "Alert", message: error.localizedDescription, options: "OK") {_ in}
            }
        case userNameTextField: newUsername = userNameTextField.text ?? ""
        case passwordTextField: newPassword = passwordTextField.text ?? ""
        case confirmpassTextField: confirmPassword = confirmpassTextField.text ?? ""
        default: return
        }
    }
}



extension EditProfileVC {
    
    // MARK: Update data
    // user auth and user data update functions
    
    func updateProfileImage(completionHandler: @escaping ()->()){
        
        // if profile image changed update image
        if profileimageChanged {
            guard let image = self.profilePicture.image else { return }
            UserDataManager.shared.uploadUserProfileImage(image: image) {
                (url, error) in
                if error == nil {
                    guard let url = url else { return }
                    let dict = ["profileURL": url.absoluteString] as [String: Any]
                    UserDataManager.shared.updateUserData(values: dict) { error in
                        if error != nil {
                            self.showAlert(title: "Alert", message: error?.localizedDescription ?? "Profile update error", options: "OK") {_ in}
                            completionHandler()
                            
                        } else {
                            completionHandler()
                        }
                    }
                } else {
                    self.showAlert(title: "Alert", message: error?.localizedDescription ?? "Upload image error", options: "OK") {_ in}
                    completionHandler()
                }
            }
        } else {
            completionHandler()
        }
    }
    
    func updatePassword(completionHandler: @escaping ()->()) {
        // Check if password fields changed
        if !newPassword.isEmpty || !confirmPassword.isEmpty {
            
            // if not equal show alert else update
            if newPassword != confirmPassword {
                self.showAlert(title: "Alert", message: "Passwords do not match", options: "OK") {_ in}
                completionHandler()
            } else {
                // NOTE: no reauthentication for password or email change implemented currently
                // Error to relogin will be shown if user didn't login recently
                // directly updating password
                user?.updatePassword(to: newPassword) { error in
                    completionHandler()
                }
            }
        } else {
            completionHandler()
        }
    }
    
    func updateEmail(completionHandler: @escaping ()->()) {
        // if email changed and is valid
        if !newEmail.isEmpty {
            // NOTE: no reauthentication for email change implemented
            // Error to relogin will be shown if user didn't login recently
            user?.updateEmail(to: newEmail) { error in
                if error != nil {
                    self.showAlert(title: "Alert",
                                   message: error?.localizedDescription ?? "Email could not be updated",
                                   options: "OK") { _ in }
                    completionHandler()
                } else {
                    completionHandler()
                }
            }
        } else {
            completionHandler()
        }
    }
    
    func updateProfileData(completionHandler: @escaping ()->()) {
        // save previous education if changed
        if self.selectedPrevEduId.count > 0 {
            let prevEdu = ["prevEdu": self.selectedPrevEduId]
            UserDataManager.shared.updateUserData(values: prevEdu) { error in
                if error != nil {
                    self.showAlert(title: "Alert", message: error?.localizedDescription ?? "Previous education update error", options: "OK") {_ in}
                    completionHandler()
                }
            }
        }
        
        if self.learningPreferenceChanged {
            // update user interest or learning preference
            var values: [String: Any] = [String: Any]()
            learningPreference.forEach {
                switch $0.facetValues.first?.facetField {
                case FacetFilterFields.teachingLang: values.updateValue($0.selectedIds, forKey: "teachingLang")
                case FacetFilterFields.topic: values.updateValue($0.selectedIds, forKey: "topic")
                case FacetFilterFields.subTopic: values.updateValue($0.selectedIds, forKey: "subTopic")
                case FacetFilterFields.educationType: values.updateValue($0.selectedIds, forKey: "educationType")
                case FacetFilterFields.subEducationType: values.updateValue($0.selectedIds, forKey: "subEducationType")
                case FacetFilterFields.timeOfTeaching: values.updateValue($0.selectedIds, forKey: "timeOfTeaching")
                case FacetFilterFields.formOfTeaching: values.updateValue($0.selectedIds, forKey: "formOfTeaching")
                case FacetFilterFields.formOfStudy: values.updateValue($0.selectedIds, forKey: "formOfStudy")
                default: return
                }
            }
            
            let interest = ["interest": values]
            UserDataManager.shared.updateUserData(values: interest) { error in
                if error == nil {
                    // remove old user related course and upload new user related course
                    UserDataManager.shared.reloadUserRelatedCourses()
                    completionHandler()
                } else {
                    self.showAlert(title: "Alert", message: error?.localizedDescription ?? "Learning preference update error", options: "OK") {_ in}
                    completionHandler()
                }
            }
        }  else {
            completionHandler()
        }
    }
}


extension EditProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Table data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.previousEducation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = self.previousEducation[indexPath.row].valueName
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: .thin)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let facetValue = self.previousEducation[indexPath.row]
        self.selectedPrevEduId = facetValue.valueId ?? ""
        self.previousEducationToggle(self)
    }
    
}


extension EditProfileVC {
    
    // MARK: Setup functions
    
    func setUpViews() {
        userNameTextField.layer.cornerRadius = 15.0
        emailTextField.layer.cornerRadius = 15.0
        passwordTextField.layer.cornerRadius = 15.0
        confirmpassTextField.layer.cornerRadius = 15.0
        design_button.layer.cornerRadius = 10.0
        
        self.profilePicture.layer.cornerRadius = min(self.profilePicture.frame.height,self.profilePicture.frame.width) / 10
        self.profilePicture.layer.borderColor = UIColor.white.cgColor
        self.profilePicture.layer.masksToBounds = true
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(EditProfileVC.openGallery(tapGesture:)))
        profilePicture.isUserInteractionEnabled = true
        profilePicture.addGestureRecognizer(tapGesture)
    }
    
    // themeing
    func colors () {
        view.backgroundColor = Theme.current.background
        emailTextField.backgroundColor = Theme.current.boxColor
        userNameTextField.backgroundColor = Theme.current.boxColor
        passwordTextField.backgroundColor = Theme.current.boxColor
        confirmpassTextField.backgroundColor = Theme.current.boxColor
        userNameTextField.textColor = Theme.current.textColor
        emailTextField.textColor = Theme.current.textColor
        passwordTextField.textColor = Theme.current.textColor
        confirmpassTextField.textColor = Theme.current.textColor
        design_button.backgroundColor = Theme.current.buttonColor
        learnBtn.backgroundColor = Theme.current.buttonColor
        cancelBtn.tintColor = Theme.current.buttonColor
        //navigationBarAppearance.tintColor = Theme.current.background
    }
    
    func setUpLearningPreference() {
        let intereset =  userData?.interest
        
        // add teaching language
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.teachingLang, selectedIds: intereset?.teachingLang))
        
        // add topic prefernce
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.topic, selectedIds: intereset?.topic))
        
        // add subtopic preference
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.subTopic, selectedIds: intereset?.subTopic))
        
        // add form of teaching
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.formOfTeaching, selectedIds: intereset?.formOfTeaching))
        
        // add form of study
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.formOfStudy, selectedIds: intereset?.formOfStudy))
        
        // add education type
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.educationType, selectedIds: intereset?.educationType))
        
        // add time of teaching
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.timeOfTeaching, selectedIds: intereset?.timeOfTeaching))
    }
    
    func getLearningPreference(fieldName: String, selectedIds: [String]?) -> LearningPreference {
        let facetValues = CoreDataManager.shared.fetchFacetValuesByFacetField(facetField: fieldName)
        return LearningPreference(title: LearningPreferenceTitles[fieldName]!, facetValues: facetValues, selectedIds: selectedIds)
    }
    
    // if user is logged in get profile data
    
    func loadUserData() {
        UserDataManager.shared.getUserData { (userdata, error) in
            if error == nil {
                self.userData = userdata
                self.profilePicture.downloadImage(url: userdata?.profileURL ?? "")
                self.userNameTextField.text = userdata?.username
                self.setUpLearningPreference()
            }
        }
        
        AuthenticationManager.shared.authenticateUser { (user, error) in
            if error == nil {
                self.user = user
                self.emailTextField.text = user?.email
            }
        }
    }
}
