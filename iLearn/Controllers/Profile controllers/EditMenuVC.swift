//
//  EditBoxVC.swift
//  iLearn
//
//  Created by Ishup Ali Khan on 03/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import Firebase

class EditMenuVC: UIViewController {
    

    lazy var signInViewController = UIStoryboard(name: "Auth",bundle: nil).instantiateViewController(withIdentifier: "SignInView") as! LogInVC
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signOutBtn(_ sender: Any) {
        self.showAlert(title: "Alert", message: "Are you sure you want to signout ?", options: "Sing out", "Cancel") { index in
            switch index {
                case 0: self.signOut()
                default: break
            }
        }
    }
    
    func signOut(){
        AuthenticationManager.shared.signOut { error in
            if error == nil {
                self.present(self.signInViewController, animated: true, completion: nil)
            } else {
                print(error ?? "Sign out error")
            }
        }
    }

}
