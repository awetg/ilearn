//
//  LearningPreferenceVC.swift
//  iLearn
//
//  Created by iosdev on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class LearningPreferenceVC: UIViewController {

    
    // MARK: Properties
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var tableview: UITableView!
    
    var prefrences:[LearningPreference] = [LearningPreference]()
    
    var updateLearningPreference: (([LearningPreference], Bool) -> Void)?
    var learningPreferenceChanged = false
    
    // MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        tableview.dataSource = self
        tableview.delegate = self
        tableview.tableHeaderView = {
            let label = UILabel()
            label.text = "Pick one or more in each section"
            label.textColor = UIColor.black
            return label
        }()
    }
    
    func setupView() {
        addBtn.setTitle("AddKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
    }
    
    
    // target for seaction header button, to expand filter options
    @objc func expandFilter(button: UIButton) {
        toggleSection(section: button.tag)
    }
    
    // toggle table section expand or hide filter options
    func toggleSection(section: Int) {
        // collect all row in the selected selection
        var indexPath = [IndexPath]()
        for row in prefrences[section].facetValues.indices {
            indexPath.append(IndexPath(row: row, section: section))
        }
        
        // toggle isExpanded of current section
        prefrences[section].isExpanded = !prefrences[section].isExpanded
        
        // if section is going to expected insert rows else delete rows
        if prefrences[section].isExpanded {
            tableview.insertRows(at: indexPath, with: .fade)
        } else {
            tableview.deleteRows(at: indexPath, with: .fade)
        }
    }
    
    // MARK: Action outlets
    
    @IBAction func addPreference(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        self.updateLearningPreference?(self.prefrences, self.learningPreferenceChanged)
    }
}

extension LearningPreferenceVC: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Table data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return prefrences.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // create UIView
        let button = UIButton(type: .custom)
        button.setTitle(prefrences[section].title, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor(hexString: "#007AFF")
        button.addTarget(self, action: #selector(expandFilter), for: .touchUpInside)
        button.tag = section
        //        button.setImage(#imageLiteral(resourceName: "drop-down-arrow"), for: .normal)
        //        button.imageEdgeInsets = UIEdgeInsets(top: 40, left: 40, bottom: 40, right: 40)
        return button
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // set number of filters as number of section
        let filter = prefrences[section]
        return filter.isExpanded ? filter.facetValues.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "filterCell", for: indexPath) as! FilterCell
        let preference = prefrences[indexPath.section]
        cell.filterOption.text = preference.facetValues[indexPath.row].valueName
        cell.checkedImg.isHidden = !preference.selectedIds.contains(preference.facetValues[indexPath.row].valueId ?? "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // get selected prefernce
        let filter = prefrences[indexPath.section]
        
        // get selcted row
        guard let selected: String = filter.facetValues[indexPath.row].valueId else { return }
        
        // if selected ids doesn't contain this preference add it
        if !filter.selectedIds.contains(selected) {
            filter.selectedIds.append(selected)
        }
        
        self.toggleSection(section: indexPath.section)
        
        self.learningPreferenceChanged = true
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

}
