//
//  DetailController.swift
//  Blops
//
//  Created by Viet Tran on 01/05/2019.
//  Copyright © 2019 Viet Tran. All rights reserved.
//

import UIKit
import Firebase

class DetailController: UIViewController {
    
    var courseDetail = Courses()
    var enrolledArray = [Courses]()
    var wishlistArray = [Courses]()

    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var prerequisite: UILabel!
    @IBOutlet weak var credit: UILabel!
    @IBOutlet weak var educationDegree: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var homeplace: UILabel!
    
    @IBOutlet weak var enrollButton: UIButton!
    @IBOutlet weak var wishlistButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        id.text = courseDetail.id
        name.text = courseDetail.name
        prerequisite.text = courseDetail.prerequisite
        credit.text = courseDetail.credits
        educationDegree.text = courseDetail.educationDegree
        type.text = courseDetail.type
        homeplace.text = courseDetail.homeplace
        
        getEnrollFromFirebase(id: courseDetail.id)
        getWishlistFromFirebase(id: courseDetail.id)
        
    }

    @IBAction func enroll(_ sender: Any) {
        let uid = "8MMnY6EfJ8cSEcC6WBweAPIe4UC2"
        let enrollDB = Database.database().reference().child("users").child(uid).child("Enroll")
        let enrollData = ["id" : id.text, "name" : name.text, "prerequisite" : prerequisite.text, "credits" : credit.text, "educationDegree" : educationDegree.text, "type" : type.text, "homeplace" : homeplace.text]
        
        enrollDB.childByAutoId().setValue(enrollData) {
            (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Enroll successfully")
            }
        }
    }
    
    @IBAction func wishlist(_ sender: Any) {
        let uid = "8MMnY6EfJ8cSEcC6WBweAPIe4UC2"
        let wishlistDB = Database.database().reference().child("users").child(uid).child("Wishlist")
        let wishlistData = ["id" : id.text, "name" : name.text, "prerequisite" : prerequisite.text, "credits" : credit.text, "educationDegree" : educationDegree.text, "type" : type.text, "homeplace" : homeplace.text]
        
        wishlistDB.childByAutoId().setValue(wishlistData) {
            (error, reference) in
            if error != nil {
                print(error!)
            } else {
                print("Wishlist successfully")
            }
        }
    }
    
    self.searchResultItems = error != nil ? [CourseDTO]() : searchResults?.results ?? [CourseDTO]()
    DispatchQueue.main.async { self.myTableView.reloadData() }
}
}

// init CourseListVC using storyboard
static func createCourseListVC(queryParams: [[String: String]]) -> CourseListVC {
    let vc = UIStoryboard(name: "Course",bundle: nil).instantiateViewController(withIdentifier: "CourseDetailVC") as! DetailController
    vc.queryParams = queryParams
    return vc
}
    
    func getEnrollFromFirebase(id : String) {
        let uid = "8MMnY6EfJ8cSEcC6WBweAPIe4UC2"
        //let enrollDB = Database.database().reference().child("users").child(uid).child("Enroll").childByAutoId()
        let enrollDB = Database.database().reference().child("users").child(uid).child("Enroll")
        
        enrollDB.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as! Dictionary<String,String>
            let course = Courses()
            print(snapshotValue)
            course.name = snapshotValue["name"]!
            course.credits = snapshotValue["credits"]!
            course.educationDegree = snapshotValue["educationDegree"]!
            course.homeplace = snapshotValue["homeplace"]!
            course.id = snapshotValue["id"]!
            course.losId = snapshotValue["id"]!
            course.prerequisite = snapshotValue["prerequisite"]!
            
            if course.id == id {
                self.enrolledArray.append(course)
                self.enrollButton.backgroundColor = UIColor.red
                self.enrollButton.isEnabled = false
                
                self.wishlistButton.isEnabled = false
                self.wishlistButton.backgroundColor = UIColor.red
            }
            
            
        }
    }
    
    func getWishlistFromFirebase(id : String) {
        let uid = "8MMnY6EfJ8cSEcC6WBweAPIe4UC2"
        
        let enrollDB = Database.database().reference().child("users").child(uid).child("Wishlist")
        
        enrollDB.observe(.childAdded) { (snapshot) in
            let snapshotValue = snapshot.value as! Dictionary<String,String>
            let course = Courses()
            print(snapshotValue)
            course.name = snapshotValue["name"]!
            course.credits = snapshotValue["credits"]!
            course.educationDegree = snapshotValue["educationDegree"]!
            course.homeplace = snapshotValue["homeplace"]!
            course.id = snapshotValue["id"]!
            course.losId = snapshotValue["id"]!
            course.prerequisite = snapshotValue["prerequisite"]!
            
            if course.id == id {
                self.wishlistArray.append(course)
                self.wishlistButton.isEnabled = false
                self.wishlistButton.backgroundColor = UIColor.red
            }
            
            
            
        }
    }
}
