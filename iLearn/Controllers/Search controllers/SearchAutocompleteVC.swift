//
//  SearchAutocompleteVC.swift
//  iLearn
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class SearchAutocompleteVC: UIViewController {
    
    // MARK: - Properties
    
    private var tableView: UITableView?
    
    let courseDataManager = CourseDataManager()
    var autoCompleteTerms = [String]()
    var searchTerm = ""
    
    var queryParams = ["start":"0","rows":"30","lang": "en", "searchType":"LO", "text": "*"]
    
    // Dispatch work item for delaying search network call to API
    var searchTask: DispatchWorkItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add table view, no storyboard for this uiviewcontroller
        self.addTableView()
        
    }
    
    func addTableView() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height + (self.navigationController?.navigationBar.frame.height ?? 0.0)
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        tableView = UITableView()
        tableView?.frame = CGRect(x: 0, y: -topBarHeight, width: displayWidth, height: displayHeight/2)
        tableView?.separatorStyle = .none
        tableView?.dataSource = self
        tableView?.delegate = self
        self.view.addSubview(tableView!)
    }
    
    // MARK: - Class functions
    
    @objc func search() {
        courseDataManager.fetchAutoComplete(searchTerm: self.searchTerm) { (suggestedTerms, error) in
            self.autoCompleteTerms = (error != nil) ? [String]() : (suggestedTerms?.loNames ?? []) + (suggestedTerms?.keywords ?? [])
            DispatchQueue.main.async {
                self.tableView?.reloadData()
                self.tableView?.flashScrollIndicators()
            }
        }
    }
}

extension SearchAutocompleteVC: UITableViewDataSource,UITableViewDelegate {
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autoCompleteTerms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = autoCompleteTerms[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let term = autoCompleteTerms[indexPath.row]
        let query = ["text": term]
        let courseListVc = CourseListVC.createCourseListVC(queryParams: query)
        self.dismiss(animated: true, completion: nil)
        self.presentingViewController?.show(courseListVc, sender: nil)
    }
}


extension SearchAutocompleteVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text?.count ?? 0 > 1 {
            self.searchTerm = searchController.searchBar.text!
            // canel prevoius search task
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.search), object: nil)
            
            // excute search task after 350ms
            self.perform(#selector(self.search), with: nil, afterDelay: 0.35)
        }
        
    }

}

extension SearchAutocompleteVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text?.count ?? 0 > 0 {
            let query = ["text": searchBar.text!]
            let courseListVc = CourseListVC.createCourseListVC(queryParams: query)
            self.dismiss(animated: true, completion: nil)
            self.presentingViewController?.show(courseListVc, sender: nil)
        }
    }
}
