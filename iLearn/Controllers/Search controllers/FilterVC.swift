//
//  FilterVC.swift
//  iLearn
//
//  Created by iosdev on 04/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {
    
    // MARK: Properties

    @IBOutlet weak var tableview: UITableView!
    
    var filters:[Filter] = [Filter]()
    
    var updateFilterClosure: (([Filter]) -> Void)?
    
    // MARK: Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        tableview.delegate = self
    }


    // target for seaction header button, to expand filter options
    @objc func expandFilter(button: UIButton) {
        toggleSection(section: button.tag)
    }
    
    // toggle table section expand or hide filter options
    func toggleSection(section: Int) {
        // collect all row in the selected selection
        var indexPath = [IndexPath]()
        for row in filters[section].facetValues.indices {
            indexPath.append(IndexPath(row: row, section: section))
        }
        
        // toggle isExpanded of current section
        filters[section].isExpanded = !filters[section].isExpanded
        
        // if section is going to expected insert rows else delete rows
        if filters[section].isExpanded {
            tableview.insertRows(at: indexPath, with: .fade)
        } else {
            tableview.deleteRows(at: indexPath, with: .fade)
        }
    }
    
    // MARK: Action outlets
    
    @IBAction func applyFilter(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        self.updateFilterClosure?(self.filters)
    }
    
    @IBAction func clearFilter(_ sender: Any) {
        filters.forEach { $0.selectedOptionId = "" }
        dismiss(animated: true, completion: nil)
        self.updateFilterClosure?(self.filters)
    }
}

extension FilterVC: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: Table data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filters.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // create UIView
        let button = UIButton(type: .custom)
        button.setTitle(filters[section].title, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor(hexString: "#007AFF")
        button.addTarget(self, action: #selector(expandFilter), for: .touchUpInside)
        button.tag = section
//        button.setImage(#imageLiteral(resourceName: "drop-down-arrow"), for: .normal)
//        button.imageEdgeInsets = UIEdgeInsets(top: 40, left: 40, bottom: 40, right: 40)
        return button
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // set number of filters as number of section
        let filter = filters[section]
        return filter.isExpanded ? filter.facetValues.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "filterCell", for: indexPath) as! FilterCell
        let filter = filters[indexPath.section]
        cell.filterOption.text = filter.facetValues[indexPath.row].valueName
        cell.checkedImg.isHidden = !(filter.selectedOptionId.count > 0 && filter.selectedOptionId == filter.facetValues[indexPath.row].valueId)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // get selected filter
        let filter = filters[indexPath.section]
        
        // get selcted row
        let selected: String = filter.facetValues[indexPath.row].valueId ?? ""
        
        // if selected row is same as previously selected option, diselected it else set selected
        filter.selectedOptionId = selected == filter.selectedOptionId ? "" : selected
        
        self.toggleSection(section: indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}
