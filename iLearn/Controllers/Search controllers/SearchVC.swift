//
//  SearchVC.swift
//  iLearn
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import CoreData

class SearchVC: UITableViewController {
    
    // MARK: Properties
    
    @IBOutlet var tableview: UITableView!
    
    // fetch result controller to get facet values from coredata
    // this will fetch topic facet values
    var fetchedResultController: NSFetchedResultsController<FacetValue>?
    
    // UISearchContorller
    var searchController = UISearchController()
    let searchResultController: UIViewController = SearchAutocompleteVC()
    
    // variable that will passed to other view contollers as segue parameters
    // segue parameter to pass to SearchSubTopicVC
    var selectedTopicFacetValue: FacetValue?
    
    
    // MARK: - functions
    
    override func viewWillAppear(_ animated: Bool) {
        self.setFetchResultController()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Search"
        tableView.separatorStyle = .none
//        setupLan()
        
        // add and customize search bar this tableview controller
        self.setupSearchController()
        
        // set fetchResultController
        self.setFetchResultController()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SearchSubTopicVC {
            vc.topicFacet = self.selectedTopicFacetValue
        }
    }
    
    
//    func setupLan() {
//        topicLabel.text = "UsernameKey".localizableString(loc: Application.shared.getLanguage())
//    }
    func setupSearchController() {
        searchController = ({
            let controller = UISearchController(searchResultsController: searchResultController)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchResultsUpdater = searchResultController as? UISearchResultsUpdating
            controller.dimsBackgroundDuringPresentation = true
            controller.searchBar.delegate = searchResultController as? UISearchBarDelegate
            
            // set search bar on header of current table view
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
    }
    
    func setFetchResultController() {
        
        let fetchRequest: NSFetchRequest<FacetValue> = FacetValue.fetchRequest()
        
        // using LIKE to filter instead of equal(==), because is not working to filter facets
        fetchRequest.predicate = NSPredicate(format: "facetField LIKE %@",  "theme_ffm")
        let sortDescriptor = NSSortDescriptor(key: "valueName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchedResultController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataManager.shared.persistentContainer.viewContext,
            sectionNameKeyPath: "valueName",
            cacheName: "topicFacetCach"
        )
        fetchedResultController!.delegate = self as NSFetchedResultsControllerDelegate
        try? fetchedResultController?.performFetch()
        self.tableview.reloadData()

    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultController?.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopicCell", for: indexPath) as! TopicCell
        guard let facetValue = self.fetchedResultController?.object(at: indexPath) else {
            fatalError("Facet value item not found from fetched result controller")
        }
        cell.setCell(topicFacet: facetValue)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let facetValue = self.fetchedResultController?.object(at: indexPath) else {
            fatalError("clicked facet value item not found from fetched result controller")
        }
        self.selectedTopicFacetValue = facetValue
        performSegue(withIdentifier: "subTopicSegue", sender: self)
    }

}

extension SearchVC: NSFetchedResultsControllerDelegate {
    
    // MARK: FetchedResultsControllerDelegate
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableview.reloadData()
    }
}
