//
//  CourseListVC.swift
//  iLearn
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class CourseListVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    // used for loading more courses when user scroll to last table cell
    // initial row fetched are 30
    var row: Int = 30
    
    // base query params that will be sent with all request
    let baseQueryParams = ["start":"0","rows":"30","lang":"en","searchType":"LO"]
    
    // passed parameter from other view contorllers will replace keys in this queryParams
    // other view controllers must pass query parameter to search the Opintopolku API
    // this queryParams is of type [String: String] so that key value can be unique and updated
    // non existing keys will be created automatically
    var queryParams = ["text":"*"]
    
    // filter queryparams with all elements having key "facetField" to filter using facet fileds
    // all filters are passed from filter viewcontroller only using Closure delegation
    var filterQueryParams: [[String: String]] = [[String: String]]()
    
    var filters:[Filter] = [Filter]()
    
    
    var searchResultItems = [CourseDTO]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        tableview.delegate = self
        self.search(queryParams: [self.baseQueryParams, self.queryParams])
        self.setUpFilters()
    }
    
    @IBAction func filterButton(_ sender: Any) {
        performSegue(withIdentifier: "filterSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? FilterVC {
            vc.filters = self.filters
            
            // closure delegate for passing selected filters from FilterVC
            // using weak self reference to avoid reference cycles
            vc.updateFilterClosure = { [weak self] filters in
                self?.filters = filters
                self?.searchWithFilters()
            }
        }
    }
    
    // init CourseListVC using storyboard
    // pass query parmas of type [String: String] to replace certain key values
    static func createCourseListVC(queryParams: [String: String]?) -> CourseListVC {
        let vc = UIStoryboard(name: "Course",bundle: nil).instantiateViewController(withIdentifier: "CourseListVC") as! CourseListVC
        queryParams?.forEach {
            vc.queryParams.updateValue($0.value, forKey: $0.key)
        }
        return vc
    }
    
    func search(queryParams: [[String: String]]?) {
        CourseDataManager().search(searchTerm: "*", queryParams: queryParams) { (searchResults, error) in
            self.searchResultItems = error != nil ? [CourseDTO]() : searchResults?.results ?? [CourseDTO]()
            DispatchQueue.main.async { self.tableview.reloadData() }
        }
    }
    
    func setUpFilters() {
        FacetFilterFields.all.forEach {
            let filter = Filter(
                title: FilterTitles[$0]!,
                options: CoreDataManager.shared.fetchFacetValuesByFacetField(facetField: $0),
                filterFacetField: $0)
            self.filters.append(filter)
        }
    }
    
    func searchWithFilters() {
        self.filterQueryParams.removeAll()
        self.filters.forEach {
            if $0.selectedOptionId.count > 0 {
                let q = ["facetFilters": $0.filterFacetField + ":" + $0.selectedOptionId]
                self.filterQueryParams.append(q)
            }
        }
        self.filterQueryParams.append(self.baseQueryParams)
        self.filterQueryParams.append(["text": self.queryParams["text"] ?? "*"])
        self.search(queryParams: self.filterQueryParams)
    }
    
    func loadMoreCourses() {
        // clone base query param to update key values
        var b = self.baseQueryParams
        b.updateValue(String(row), forKey: "start")
        b.updateValue(String(row + 30), forKey: "rows")
        self.row += 30
        
        // uquery using filter query params if user used filter else use base plus passed querys
        let querys = self.filterQueryParams.count > 0 ? self.filterQueryParams : [b,queryParams]
        
        CourseDataManager().search(searchTerm: "*", queryParams: querys) { (searchResults, error) in
            if error == nil {
                self.searchResultItems.append(contentsOf: searchResults?.results ?? [CourseDTO]())
                DispatchQueue.main.async { self.tableview.reloadData() }
            }
        }
    }
}

extension CourseListVC: UITableViewDataSource,UITableViewDelegate {
    
     // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return searchResultItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "searchCourseTableCell", for: indexPath) as! CourseTableCell
        cell.setItem(course: searchResultItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == searchResultItems.count - 1 {
            self.loadMoreCourses()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let course = searchResultItems[indexPath.row]
        let courseDetail = CourseDetail(course: course)
        let vc = CourseDetailVC.createCourseDetailVC(course: courseDetail)
        //self.window?.rootViewController?.navigationController?.present(vc, animated: false, completion: nil)
//        self.present(vc, animated: false, completion: nil)
        self.navigationController?.show(vc, sender: self)
    }
}
