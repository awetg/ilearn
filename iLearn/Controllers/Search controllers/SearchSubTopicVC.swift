//
//  SearchSubTopicVC.swift
//  iLearn
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class SearchSubTopicVC:  UIViewController {
    
    // variable that will be recieved from SearchVC controlelr
    // this variable wil be recieved as segue parameter
    var topicFacet: FacetValue?
    
    var subTopicFacetValueItems = [FacetValue]()
    var topicCourses = [CourseDTO]()
    
    var queryParams = ["start":"0","rows":"30","lang":"en","searchType":"LO","text":"*"]

    @IBOutlet weak var courseCollectionView: UICollectionView!
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set table and collection view data source and delegate
        courseCollectionView.dataSource = self
        courseCollectionView.delegate = self
        tableview.dataSource = self
        tableview.delegate = self
        
        // get subtopic facet value items
        subTopicFacetValueItems = CoreDataManager.shared.fetchChildFacetValues(parentId: self.topicFacet?.valueId ?? "")
        
        // get courses by selected topic
        self.getTopicCourses()
    }
    
    @IBAction func viewAll(_ sender: UIButton) {
        let query = ["facetFilters": "theme_ffm:" + (topicFacet?.valueId ?? "")]
        let courseListVC = CourseListVC.createCourseListVC(queryParams: query)
        show(courseListVC, sender: nil)
    }
    
    func getTopicCourses() {
        self.queryParams.updateValue("theme_ffm:" + (topicFacet?.valueId ?? ""), forKey: "facetFilters")
        CourseDataManager().search(searchTerm: "*", queryParams: [self.queryParams]) { (searchResults, error) in
            self.topicCourses = error != nil ? [CourseDTO](): searchResults?.results ?? [CourseDTO]()
            DispatchQueue.main.async { self.courseCollectionView.reloadData() }
        }
    }
}

extension SearchSubTopicVC: UITableViewDataSource,UITableViewDelegate {
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subTopicFacetValueItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  UITableViewCell()
        cell.textLabel?.text = subTopicFacetValueItems[indexPath.row].valueName
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: .thin)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedFacet = subTopicFacetValueItems[indexPath.row]
        let query = ["facetFilters": "topic_ffm:" + (selectedFacet.valueId ?? "")]
        let courseListVC = CourseListVC.createCourseListVC(queryParams: query)
        show(courseListVC, sender: nil)
    }

}

extension SearchSubTopicVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.topicCourses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = courseCollectionView.dequeueReusableCell(withReuseIdentifier: "courseCollectionCell", for: indexPath) as! CourseCollectionCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.gray.cgColor
        cell.layer.cornerRadius = 3
        cell.setCourseDTO(course: topicCourses[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let course = topicCourses[indexPath.row]
        let courseDetail = CourseDetail(course: course)
        let vc = CourseDetailVC.createCourseDetailVC(course: courseDetail)
        self.navigationController?.show(vc, sender: self)
    }
    
    
}
