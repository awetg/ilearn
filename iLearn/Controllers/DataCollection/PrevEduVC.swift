//
//  PrevEduVC.swift
//  iLearn
//
//  Created by Utsab Kharel on 05/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import Firebase

class PrevEduVC: UITableViewController {

    @IBOutlet weak var doneOutlet: UIButton!
    
    private var selectedPrevEdu: String?
    
    var selectedRows = Set<IndexPath>()
    
    private let previousEducation = CoreDataManager.shared.prerequisiteFacetVAlues()

    override func viewDidLoad() {
        super.viewDidLoad()
        doneOutlet.isEnabled = false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return previousEducation.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "prevEduViewCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = previousEducation[indexPath.row].valueName
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
       self.selectedPrevEdu = previousEducation[indexPath.row].valueId ?? ""
        
        // Remove check mark if more than one row is selected
       self.selectedRows.insert(indexPath)
        if(selectedRows.count > 1){
            for row in selectedRows{
                tableView.cellForRow(at: row)?.accessoryType = UITableViewCell.AccessoryType.none
            }
        }
        
        
        // if clicked once it will tick but selecting twice will remove it
        if (cell!.isSelected == true)
        {
            cell!.isSelected = false
            if cell!.accessoryType == UITableViewCell.AccessoryType.none
            
        {
            cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        else
        {
            cell!.accessoryType = UITableViewCell.AccessoryType.none
        }
            
        }
        
        // Disable button unless something is selected
        if(self.selectedPrevEdu == nil){
            doneOutlet.isEnabled = false
        }else {
            doneOutlet.isEnabled = true

        }
        
        print("This is selected PrevEdu:   ",self.selectedPrevEdu ?? "")

    }
    
    @IBAction func doneButton(_ sender: Any) {
        performSegue(withIdentifier: "intersetSegue", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is DataCollectionVC {
           // self.show(vc, sender: nil)
        }
    
    let prevEdu = self.selectedPrevEdu ?? ""
        UserDataManager.shared.updateUserData(values: ["PrevEdu": prevEdu]) { e in
            print(e as Any)
    }
        
    }
}
