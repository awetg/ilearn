//
//  DataCollectionVC.swift
//  iLearn
//
//  Created by Utsab Kharel on 07/05/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit

class DataCollectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var doneOutlet: UIBarButtonItem!
    @IBOutlet var tableview: UITableView!
    var learningPreference = [LearningPreference]()
    var userData: UserData?
    var currenIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doneOutlet.isEnabled = false
        tableview.delegate = self
        tableview.dataSource = self
        self.setUpLearningPreference()
        self.navigationItem.title = learningPreference[currenIndex].title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print("row num", learningPreference[currenIndex].facetValues.count)
        return learningPreference[currenIndex].facetValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        cell.textLabel?.text =  learningPreference[currenIndex].facetValues[indexPath.row].valueName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        guard let selected = learningPreference[currenIndex].facetValues[indexPath.row].valueId else { return }
        
        if learningPreference[currenIndex].selectedIds.contains(selected) {
            learningPreference[currenIndex].selectedIds = learningPreference[currenIndex].selectedIds.filter { $0 != selected}
            cell!.accessoryType = UITableViewCell.AccessoryType.none
        } else {
            learningPreference[currenIndex].selectedIds.append(selected)
            cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        
        doneOutlet.isEnabled = learningPreference[currenIndex].selectedIds.isEmpty ? false : true
    }
    
    
    
    
    @IBAction func doneButton(_ sender: Any) {
    
    if currenIndex == learningPreference.count - 1 {
        self.uploadProfileData {}
        let homeViewController = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
        self.present(homeViewController, animated: true, completion: nil)
        } else {
            currenIndex += 1
            tableview.reloadData()
            print("Navigation title:",learningPreference[currenIndex].title)
            self.navigationItem.title = learningPreference[currenIndex].title
        }
        
    }
    
    
    
    func setUpLearningPreference() {
        
        // add teaching language
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.teachingLang, selectedIds: nil))
        
        // add topic prefernce
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.topic, selectedIds: nil))
        
        // add subtopic preference
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.subTopic, selectedIds: nil))
        
        // add form of teaching
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.formOfTeaching, selectedIds: nil))
        
        // add form of study
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.formOfStudy, selectedIds: nil))
        
        // add education type
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.educationType, selectedIds: nil))
        
        // add time of teaching
        self.learningPreference.append(getLearningPreference(fieldName: FacetFilterFields.timeOfTeaching, selectedIds: nil))
    }
    
    func getLearningPreference(fieldName: String, selectedIds: [String]?) -> LearningPreference {
        let facetValues = CoreDataManager.shared.fetchFacetValuesByFacetField(facetField: fieldName)
        return LearningPreference(title: LearningPreferenceTitles[fieldName]!, facetValues: facetValues, selectedIds: selectedIds)
    }
    
    
        func uploadProfileData(completionHandler: @escaping ()->()) {
    
    
    
            // update user interest or learning preference
            var values: [String: Any] = [String: Any]()
            learningPreference.forEach {
                switch $0.facetValues.first?.facetField {
                case FacetFilterFields.teachingLang: values.updateValue($0.selectedIds, forKey: "teachingLang")
                case FacetFilterFields.topic: values.updateValue($0.selectedIds, forKey: "topic")
                case FacetFilterFields.subTopic: values.updateValue($0.selectedIds, forKey: "subTopic")
                case FacetFilterFields.educationType: values.updateValue($0.selectedIds, forKey: "educationType")
                case FacetFilterFields.subEducationType: values.updateValue($0.selectedIds, forKey: "subEducationType")
                case FacetFilterFields.timeOfTeaching: values.updateValue($0.selectedIds, forKey: "timeOfTeaching")
                case FacetFilterFields.formOfTeaching: values.updateValue($0.selectedIds, forKey: "formOfTeaching")
                case FacetFilterFields.formOfStudy: values.updateValue($0.selectedIds, forKey: "formOfStudy")
                default: return
                }
            }
            
            UserDataManager.shared.updateUserData(values: ["Interests": values]) { e in
                print(e as Any)
            }
    
    }
    
}

