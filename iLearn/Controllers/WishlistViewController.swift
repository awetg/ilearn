//
//  WishlistViewController.swift
//  Blops
//
//  Created by Viet Tran on 01/05/2019.
//  Copyright © 2019 Viet Tran. All rights reserved.
//

import UIKit
import Firebase

class WishlistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var wishlistTableView: UITableView!
    
    var courseArray = [CourseDTO]()
    
    override func viewWillAppear(_ animated: Bool) {
        getWishlistFromFirebase()
        wishlistTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wishlistTableView.delegate = self
        wishlistTableView.dataSource = self
        
        wishlistTableView.register(UINib(nibName: "CourseCustomCell", bundle: nil), forCellReuseIdentifier: "courseCustomCell")
        
        getWishlistFromFirebase()
        wishlistTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = wishlistTableView.dequeueReusableCell(withIdentifier: "searchCourseTableCell", for: indexPath) as! CourseTableCell
        cell.setItem(course: courseArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let course = courseArray[indexPath.row]
        let courseDetail = CourseDetail(course: course)
        let vc = CourseDetailVC.createCourseDetailVC(course: courseDetail)
        //self.window?.rootViewController?.navigationController?.present(vc, animated: false, completion: nil)
//        self.present(vc, animated: false, completion: nil)
        self.navigationController?.show(vc, sender: self)
    }
    
    
    func getWishlistFromFirebase() {
        courseArray.removeAll()
        UserDataManager.shared.getUserData { (userData, error) in
            userData?.wishlisted?.forEach { (k,v) in
                
                self.courseArray.append(v)
            }
            self.wishlistTableView.reloadData()
        }
    }

    

}
