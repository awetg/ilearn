//
//  LogInVC.swift
//  iLearn
//
//  Created by iosdev on 19/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import Firebase

class LogInVC: UIViewController {

    
    @IBOutlet weak var loginTitle: UINavigationItem!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLan()
    }
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var dontHave: UIButton!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        colors()
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        logUser(withEmail: email, password: password)
      
    }
    
    func setupLan() {
        
        navigationItem.title = "LoginKey".localizableString(loc: Application.shared.getLanguage())
        emailTextField.text = "EmailKey".localizableString(loc: Application.shared.getLanguage())
        passwordTextField.text = "PasswordKey".localizableString(loc: Application.shared.getLanguage())
        dontHave.setTitle("letMeKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
        
        loginBtn.setTitle("LoginKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
        
    }
    
    func setupView() {
        emailTextField.layer.cornerRadius = 10.0
        emailTextField.layer.masksToBounds = true
        passwordTextField.layer.cornerRadius = 10.0
        passwordTextField.layer.masksToBounds = true
        loginBtn.layer.cornerRadius = 10.0
    }
    
    
    func logUser(withEmail email: String, password: String){
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            
            if let error = error {
                print("Failed to sign user in with error:", error.localizedDescription)
                
                let alertController = UIAlertController(title: "Alert", message: error.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {action in print("OK tapped")} )
                
                alertController.addAction(defaultAction)
                self.present(alertController,animated: true,completion: nil)
                return
            }
            print ("Login Sucessfully")
            
          
            let mainVC = UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "MainTabBarVC") as! MainTabBarVC
            self.present(mainVC, animated: true, completion: nil)
            
        }
    }
}

extension LogInVC {
    
    func colors () {
        view.backgroundColor = Theme.current.background
        emailTextField.backgroundColor = Theme.current.boxColor
        passwordTextField.backgroundColor = Theme.current.boxColor
        emailTextField.textColor = Theme.current.textColor
        passwordTextField.textColor = Theme.current.textColor
        loginBtn.backgroundColor = Theme.current.buttonColor
        dontHave.tintColor = Theme.current.buttonColor
        loginLabel.textColor = Theme.current.textColor
        //navigationBarAppearance.tintColor = Theme.current.background
    }
}
