//
//  SignUpVC.swift
//  iLearn
//
//  Created by iosdev on 19/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import Firebase

class SignUpVC: UIViewController {

    @IBOutlet weak var registerTitle: UINavigationItem!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    
    @IBOutlet weak var labella: UILabel!
    @IBOutlet weak var design_btn: UIButton!
    @IBOutlet weak var haveAcc: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLan()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        colors()
    }
    @IBAction func registerButton(_ sender: UIButton) {
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let repeatPassword = repeatPasswordTextField.text else { return }
        guard let username = usernameTextField.text else { return }
        
        
        //Validate email
        func validateEmail(enteredEmail:String) -> Bool {
            
            let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
            return emailPredicate.evaluate(with: enteredEmail)
            
        }
        
        if validateEmail(enteredEmail: email) == false{
            alertMessage(userMessage: "Email badly formatted")
        }
        
        
        //Validate require field
        if(email.isEmpty) ||
            (password.isEmpty) ||
            (repeatPassword.isEmpty) ||
            (username.isEmpty){
            alertMessage(userMessage: "Required filed must be filled")
            return
        }
        
        // Validate password
        if((passwordTextField.text?.elementsEqual(repeatPasswordTextField.text!))! != true){
            alertMessage(userMessage: "Make sure password matches")
            return
        }
        
        createuser(withEmail: email, password: password, username: username)
        
       

    }
    
    func setupView() {
    usernameTextField.layer.cornerRadius = 10.0
    usernameTextField.layer.masksToBounds = true
    emailTextField.layer.cornerRadius = 10.0
    emailTextField.layer.masksToBounds = true
    passwordTextField.layer.cornerRadius = 10.0
    passwordTextField.layer.masksToBounds = true
    repeatPasswordTextField.layer.cornerRadius = 10.0
    repeatPasswordTextField.layer.masksToBounds = true
    design_btn.layer.cornerRadius = 10.0
    }
    
    
    func setupLan() {
        
        navigationItem.title = "RegisterKey".localizableString(loc: Application.shared.getLanguage())
        emailTextField.text = "EmailKey".localizableString(loc: Application.shared.getLanguage())
        usernameTextField.text = "UsernameKey".localizableString(loc: Application.shared.getLanguage())
        passwordTextField.text = "PasswordKey".localizableString(loc: Application.shared.getLanguage())
        repeatPasswordTextField.text = "Repeat password".localizableString(loc: Application.shared.getLanguage())
        haveAcc.setTitle("HaveKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
    design_btn.setTitle("RegisterKey".localizableString(loc: Application.shared.getLanguage()), for: .normal)
        
    }
    //Alert function
    @objc func alertMessage(userMessage:String){
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Alert", message: userMessage, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {action in print("OK tapped")} )
            
            alertController.addAction(defaultAction)
            self.present(alertController,animated: true,completion: nil)
        }
        
    }
    
    //Register user
    func createuser(withEmail email:String, password:String, username: String){
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                self.alertMessage(userMessage: "\(error.localizedDescription)")
                print("Failed to sign user up with error: ", error.localizedDescription)
                return
            }
            
            guard let uid = result?.user.uid else { return }
            
            let values = ["email": email , "username": username]
            
            // Store data in Database with branch name(users), subbranch(uid)
            // UID contains email and password
            
            Database.database().reference().child("users").child(uid).updateChildValues(values, withCompletionBlock: {(error, ref) in
                
                
                if let error = error {
                    print("Failed to update database values with error: ", error.localizedDescription)
                    return
                }
               
                
                let prevEduVC = UIStoryboard(name: "DataCollection",bundle: nil).instantiateViewController(withIdentifier: "prevEducation") as! PrevEduVC
                //self.present(prevEduVC, animated: true, completion: nil)
                self.navigationController?.pushViewController(prevEduVC, animated: true)
            })
        }
        
    }
}

extension SignUpVC {
    func colors () {
        view.backgroundColor = Theme.current.background
        emailTextField.backgroundColor = Theme.current.boxColor
        usernameTextField.backgroundColor = Theme.current.boxColor
        passwordTextField.backgroundColor = Theme.current.boxColor
        repeatPasswordTextField.backgroundColor = Theme.current.boxColor
        usernameTextField.textColor = Theme.current.textColor
        emailTextField.textColor = Theme.current.textColor
        passwordTextField.textColor = Theme.current.textColor
        repeatPasswordTextField.textColor = Theme.current.textColor
        design_btn.backgroundColor = Theme.current.buttonColor
        labella.textColor = Theme.current.textColor
        haveAcc.tintColor = Theme.current.buttonColor
        //navigationBarAppearance.tintColor = Theme.current.background
    }
}
