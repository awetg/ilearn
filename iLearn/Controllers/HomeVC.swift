//
//  HomeVC.swift
//  iLearn
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import UIKit
import CoreData

class HomeVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    var recommendation: [Recommendation] = []
    var sliders: [SliderItem] = [
        SliderItem(title: "Basic education", image: #imageLiteral(resourceName: "basic"), facetFilter: "educationType_ffm:et01.03.01"),
        SliderItem(title: "Bachelor's degree", image: #imageLiteral(resourceName: "bach"), facetFilter: "educationType_ffm:et01.04.01"),
        SliderItem(title: "Master’s degree", image: #imageLiteral(resourceName: "master"), facetFilter: "educationType_ffm:et01.05.02")
    ]
    
    var timer = Timer()
    var pageCounter = 0
    
    var fetchedResultController: NSFetchedResultsController<Course>?
    
    var userData: UserData?
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        var frame = tableview.frame
        frame.size.height = tableview.contentSize.height
        tableview.frame = frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchRecommendedCourses()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        tableview.dataSource = self
        tableview.delegate = self
        tableview.separatorStyle = .none
        pageController.numberOfPages = sliders.count
        pageController.currentPage = 0
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(self.autoScrollImages), userInfo: nil, repeats: true)
        }

        UserDataManager.shared.getUserData { (userdata, error) in self.userData = userdata}
        
        setupFetchResultController()
        fetchRecommendedCourses()
        
    }
    
    @objc func autoScrollImages() {
        if pageCounter < sliders.count {
            let indexPath = IndexPath.init(item: pageCounter, section: 0)
            sliderCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            pageController.currentPage = pageCounter
            pageCounter += 1
        } else {
            pageCounter = 0
            let indexPath = IndexPath.init(item: pageCounter, section: 0)
            sliderCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            pageController.currentPage = pageCounter
            pageCounter = 1
        }
    }
    
    func setupFetchResultController() {
        // user NSPredicate to get specific facet value or CoreDataManager functions
        let fetchRequest: NSFetchRequest<Course> = Course.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchedResultController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataManager.shared.persistentContainer.viewContext,
            sectionNameKeyPath: "name",
            cacheName: "CourseCache"
        )
        fetchedResultController!.delegate = self as NSFetchedResultsControllerDelegate
    }
    
}


// NSFetchedResultsControllerDelegate

extension HomeVC: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        fetchedResultController?.fetchRequest.predicate = nil
        fetchRecommendedCourses()
    }
}


// UITableview datasource and delegate extension

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recommendation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recommendationCell", for: indexPath) as! RecommendationCell
        cell.setItem(recommendation: recommendation[indexPath.row])
        cell.selectionStyle = .none
        cell.delegateVC = self
        return cell
    }
}


// Recommendation functions extension
extension HomeVC {
    
    // add recommended course and title to recommendation array
    func addRecommendation(title: String, courses: [Course]?) {
        if courses?.count ?? 0 > 0 {
            recommendation.append(Recommendation(title: title, courses: courses ?? [Course]()))
        }
    }
    
    // fetch recommended courses and add then to recommendation array
    func fetchRecommendedCourses() {
        
        recommendation.removeAll()
        
        // fetch all user related course that are save to coredata
        try? fetchedResultController?.performFetch()
        
        // fetch courses with ongoing application
        // not using predicate because NSPredicate with bool keypath is not working, filering downloaded course directly
        let ongoingCourses = fetchedResultController?.fetchedObjects?.filter { $0.asOngoing  }
        addRecommendation(title: "Ongoing Applications", courses: ongoingCourses)
        
        // filter courses with upcoming application
        let upcommingCourses = fetchedResultController?.fetchedObjects?.filter { $0.nextApplicationPeriodStarts?.count ?? 0 > 0 }
        addRecommendation(title: "Upcomming Applications", courses: upcommingCourses)
        
        // filter course with no prerequiste or prerequiste equal to user's previous education
        let nextStepCourses = fetchedResultController?.fetchedObjects?.filter {
            $0.prerequisite == "eipk" || userData?.prevEdu?.contains($0.prerequisite ?? "") ?? false
        }
        addRecommendation(title: "Your next step", courses: nextStepCourses)
        
        // filter degree type courses
        let degreeCourses = fetchedResultController?.fetchedObjects?.filter { $0.type == "TUTKINTO" }
        addRecommendation(title: "Degree courses", courses: degreeCourses)
        
        // filter polytechnic courses
        let polytechnicCourse = fetchedResultController?.fetchedObjects?.filter { $0.type == "KORKEAKOULU"}
        addRecommendation(title: "Polytechnic courses", courses: polytechnicCourse)
        
        // filter education courses
        let educationCourses = fetchedResultController?.fetchedObjects?.filter { $0.type == "KOULUTUS"}
        addRecommendation(title: "Education courses", courses: educationCourses)
        
        // add all user related courses as recommended
        try? fetchedResultController?.performFetch()
        addRecommendation(title: "Recommended", courses: fetchedResultController?.fetchedObjects)
        
        tableview.reloadData()
        
    }

}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sliders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollectionCell", for: indexPath) as! SliderCollectionCell
        cell.setItem(slider: sliders[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sliderItem = sliders[indexPath.row]
        let query = ["facetFilters": sliderItem.facetFilter]
        let courseListVC = CourseListVC.createCourseListVC(queryParams: query)
        show(courseListVC, sender: nil)
    }
}

extension HomeVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = sliderCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
