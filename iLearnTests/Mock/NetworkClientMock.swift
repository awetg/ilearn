//
//  NetworkClientMock.swift
//  iLearnTests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
@testable import iLearn

class NetworkclientMock: NetworkClientProtocol {
    // internal variables
    // must be private for setting to ensure it is not set from unit test
    private (set) var baseUrl: String
    
    // mock data
    var mockResponse: Response?
    
    init(baseUrl: String) {
        self.baseUrl = baseUrl
    }
    
    func execute(_ request: RequestProtocol, completionHandler: @escaping (ResponseProtocol) -> ()) {
        if let response = mockResponse {
            completionHandler(response)
        } else {
            let result = Response.Result<Data>.failure(NetworkError.networkMock)
            completionHandler(Response(httpResponse: nil, request: request, data: nil, result: result))
        }
    }
    
}
