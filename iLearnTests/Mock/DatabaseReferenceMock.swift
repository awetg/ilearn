//
//  DatabaseReferenceMock.swift
//  iLearnTests
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Firebase

class DatabaseReferenceMock: DatabaseReference{
    
    
    override func child(_ pathString: String) -> DatabaseReference {
        return self
    }
    
    override func observe(_ eventType: DataEventType, with block: @escaping (DataSnapshot) -> Void) -> UInt {
        let mockResponse: DataSnapshot = MockSnapshot()
        block(mockResponse)
        return UInt()
    }
    
    override func observeSingleEvent(of eventType: DataEventType, with block: @escaping (DataSnapshot) -> Void) {
        let mockResponse: DataSnapshot = MockSnapshot()
        block(mockResponse)
    }
}


class MockSnapshot: DataSnapshot {
    
    override var value: Any? {
        return UserDataResponseMock.userDataMock.jsonString().data(using: .utf8)
    }
}
