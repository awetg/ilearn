//
//  TypeSafeJson.swift
//  iLearnTests
//
//  Created by iosdev on 23/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation

import Foundation

public enum JSONValue {
    case string(String)
    case int(Int)
    case double(Double)
    case bool(Bool)
    case object([String: JSONValue])
    case array([JSONValue])
}

extension JSONValue: Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let string): try container.encode(string)
        case .int(let int): try container.encode(int)
        case .double(let double): try container.encode(double)
        case .bool(let bool): try container.encode(bool)
        case .object(let object): try container.encode(object)
        case .array(let array): try container.encode(array)
        }
    }
}

public extension JSONValue {
    func jsonString(file: StaticString =  #file, line: UInt = #line) -> String {
        do {
            guard let value = try String(data: JSONEncoder().encode(self), encoding: .utf8) else {
                fatalError("Error in test data! String could not be parsed from data", file: file, line: line)
            }
            return value
        } catch {
            fatalError("Error in test data! \(error.localizedDescription)", file: file, line: line)
        }
    }
}

extension JSONValue: ExpressibleByStringLiteral {
    public init(stringLiteral value: String) {
        self = .string(value)
    }
}

extension JSONValue: ExpressibleByIntegerLiteral {
    public init(integerLiteral value: Int) {
        self = .int(value)
    }
}

extension JSONValue: ExpressibleByFloatLiteral {
    public init(floatLiteral value: Double) {
        self = .double(value)
    }
}

extension JSONValue: ExpressibleByBooleanLiteral {
    public init(booleanLiteral value: Bool) {
        self = .bool(value)
    }
}

extension JSONValue: ExpressibleByArrayLiteral {
    public init(arrayLiteral elements: JSONValue...) {
        self = .array(elements)
    }
}

extension JSONValue: ExpressibleByDictionaryLiteral {
    public init(dictionaryLiteral elements: (String, JSONValue)...) {
        self = .object([String: JSONValue](uniqueKeysWithValues: elements))
    }
}

public struct UserDataResponseMock {
    static let userDataMock: JSONValue = [
        "email" : "dog@gmail.com",
        "interest" : [
            "educationType" : [ "et01.01" ],
            "formOfStudy" : [ "opetusmuotokk_1" ],
            "formOfTeaching" : [ "opetuspaikkakk_1" ],
            "subEducationType" : [ "et01.01.01" ],
            "subTopic" : [ "teemat_1.aiheet_1" ],
            "teachingLang" : [ "EN" ],
            "timeOfTeaching" : [ "opetusaikakk_1" ],
            "topic" : [ "teemat_1" ]
        ],
        "prevEdu" : [ "Bachelor's degree (UAS)" ],
        "username" : "dog"
    ]
}

public struct CourseResponseMock {
    static let  emptyJsonValue: JSONValue = []
    static let searchResultMock: JSONValue = [
        "results": [
            [
                "id": "1.2.246.562.17.45264075073",
                "name": "Lukion jalkapallolinja",
                "lopIds": [
                    "1.2.246.562.10.24634059448"
                ],
                "lopNames": [
                    "Pietarsaaren lukio"
                ],
                "prerequisite": "Peruskoulu",
                "prerequisiteCode": "PK",
                "parentId": "",
                "losId": "1.2.246.562.17.45264075073",
                "asOngoing": false,
                "nextApplicationPeriodStarts": [],
                "type": "KOULUTUS",
                "credits": "75 course units",
                "educationType": "",
                "educationDegree": "General upper secondary education",
                "educationDegreeCode": "koulutusasteoph2002_31",
                "homeplace": "Pietarsaari",
                "childName": "",
                "subjects": CourseResponseMock.emptyJsonValue,
                "responsibleProvider": ""
            ],
            [
                "id": "1.2.246.562.17.23480834011",
                "name": "IB-programme (special educational mission)",
                "lopIds": [
                    "1.2.246.562.10.85047244869"
                ],
                "lopNames": [
                    "Imatran yhteislukio"
                ],
                "prerequisite": "Peruskoulu",
                "prerequisiteCode": "PK",
                "parentId": "",
                "losId": "1.2.246.562.17.23480834011",
                "asOngoing": false,
                "nextApplicationPeriodStarts": [],
                "type": "KOULUTUS",
                "credits": "75 course units",
                "educationType": "",
                "educationDegree": "General upper secondary education",
                "educationDegreeCode": "koulutusasteoph2002_31",
                "homeplace": "Imatra",
                "childName": "",
                "subjects": CourseResponseMock.emptyJsonValue,
                "responsibleProvider": ""
            ]
        ],
        "articleresults": [],
        "providerResults": [],
        "totalCount": 15449,
        "teachingLangFacet": [
            "facetValues": [
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "CA",
                    "valueName": "Catalan",
                    "count": 1,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "DE",
                    "valueName": "German",
                    "count": 22,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "EN",
                    "valueName": "English",
                    "count": 1153,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "ES",
                    "valueName": "Spanish, Castilian",
                    "count": 11,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "FI",
                    "valueName": "Finnish",
                    "count": 9408,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "FR",
                    "valueName": "French",
                    "count": 5,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "HE",
                    "valueName": "Hebrew",
                    "count": 1,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "IT",
                    "valueName": "Italian",
                    "count": 5,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "JA",
                    "valueName": "Japanese",
                    "count": 5,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "KO",
                    "valueName": "Korean",
                    "count": 1,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "LA",
                    "valueName": "Latin",
                    "count": 3,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "PL",
                    "valueName": "Polish",
                    "count": 1,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "PT",
                    "valueName": "Portuguese",
                    "count": 4,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "RU",
                    "valueName": "Russian",
                    "count": 11,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "SE",
                    "valueName": "Northern Sami",
                    "count": 1,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "SO",
                    "valueName": "Somali",
                    "count": 1,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "SV",
                    "valueName": "Swedish",
                    "count": 840,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "VK",
                    "valueName": "Sign language",
                    "count": 6,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "teachingLangCode_ffm",
                    "valueId": "ZH",
                    "valueName": "Chinese",
                    "count": 2,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "filterFacet": [
            "facetValues": []
        ],
        "appStatusFacet": [
            "facetValues": [
                [
                    "facetField": "appStatus",
                    "valueId": "ongoing",
                    "valueName": "ongoing",
                    "count": 3619,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "appStatus",
                    "valueId": "upcoming",
                    "valueName": "2019|spring",
                    "count": 1017,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "appStatus",
                    "valueId": "upcomingLater",
                    "valueName": "2019|fall",
                    "count": 139,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "edTypeFacet": [
            "facetValues": [
                [
                    "facetField": "educationType_ffm",
                    "valueId": "et01.01",
                    "valueName": "General upper secondary school",
                    "count": 578,
                    "childValues": [
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.01.01",
                            "valueName": "General upper secondary school for young",
                            "count": 0,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.01"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.01.02",
                            "valueName": "General upper secondary school for adults",
                            "count": 17,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.01"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "educationType_ffm",
                    "valueId": "et01.03",
                    "valueName": "Vocational qualifications",
                    "count": 2692,
                    "childValues": [
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.03.001",
                            "valueName": "Dual qualification",
                            "count": 267,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.03"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.03.01",
                            "valueName": "Vocational upper secondary qualification",
                            "count": 1739,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.03"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.03.02",
                            "valueName": "Vocational upper secondary qualification as special needs education",
                            "count": 157,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.03"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.03.03",
                            "valueName": "Further vocational qualification",
                            "count": 598,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.03"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.03.04",
                            "valueName": "Specialist vocational qualification",
                            "count": 196,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.03"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.03.05",
                            "valueName": "",
                            "count": 2,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.03"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "educationType_ffm",
                    "valueId": "et01.04",
                    "valueName": "University of Applied Sciences",
                    "count": 1276,
                    "childValues": [
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.04.01",
                            "valueName": "UAS Bachelor's degree",
                            "count": 866,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.04"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.04.02",
                            "valueName": "UAS Master's degree",
                            "count": 309,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.04"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.04.03",
                            "valueName": "Open UAS education",
                            "count": 101,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.04"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "educationType_ffm",
                    "valueId": "et01.05",
                    "valueName": "University",
                    "count": 5343,
                    "childValues": [
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.05.05",
                            "valueName": "Lower + Higher",
                            "count": 318,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.05"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.05.01",
                            "valueName": "Lower (Bachelor's degree etc.)",
                            "count": 44,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.05"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.05.02",
                            "valueName": "Higher (Master's degree etc.)",
                            "count": 684,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.05"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.05.04",
                            "valueName": "Third-cycle degree",
                            "count": 306,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.05"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et01.05.03",
                            "valueName": "Open university education",
                            "count": 3991,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et01.05"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "educationType_ffm",
                    "valueId": "et02",
                    "valueName": "Other study options",
                    "count": 634,
                    "childValues": [
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.01",
                            "valueName": "Preparatory education after basic education",
                            "count": 148,
                            "childValues": [
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.01.01",
                                    "valueName": "Voluntary additional basic education",
                                    "count": 44,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.01"
                                ],
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.01.02",
                                    "valueName": "Vocational Start",
                                    "count": 0,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.01"
                                ],
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.01.03",
                                    "valueName": "",
                                    "count": 0,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.01"
                                ],
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.01.04",
                                    "valueName": "Preparatory education for immigrants for upper secondary education",
                                    "count": 18,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.01"
                                ],
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.01.06",
                                    "valueName": "Preparatory instruction and guidance for vocational education and training",
                                    "count": 86,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.01"
                                ]
                            ],
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.02",
                            "valueName": "",
                            "count": 0,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.03",
                            "valueName": "Basic education for adults",
                            "count": 21,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.05",
                            "valueName": "Folk high school long courses",
                            "count": 325,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.08",
                            "valueName": "",
                            "count": 0,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.09",
                            "valueName": "Other polytechnic/UAS adult education",
                            "count": 0,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.10",
                            "valueName": "University-level continuing education",
                            "count": 0,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.11",
                            "valueName": "Vocational teacher education",
                            "count": 14,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "et02"
                        ],
                        [
                            "facetField": "educationType_ffm",
                            "valueId": "et02.12",
                            "valueName": "Erityisopetus ja valmentava",
                            "count": 126,
                            "childValues": [
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.12.01",
                                    "valueName": "Training preparing students for work and independent living",
                                    "count": 74,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.12"
                                ],
                                [
                                    "facetField": "educationType_ffm",
                                    "valueId": "et02.12.02",
                                    "valueName": "Preparatory instruction and guidance for vocational education and training as special needs education",
                                    "count": 52,
                                    "childValues": CourseResponseMock.emptyJsonValue,
                                    "parentId": "et02.12"
                                ]
                            ],
                            "parentId": "et02"
                        ]
                    ],
                    "parentId": ""
                ]
            ]
        ],
        "prerequisiteFacet": [
            "facetValues": [
                [
                    "facetField": "prerequisites",
                    "valueId": "ak",
                    "valueName": "Bachelor’s degree (UAS)",
                    "count": 6693,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "prerequisites",
                    "valueId": "at",
                    "valueName": "Vocational qualification",
                    "count": 6584,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "prerequisites",
                    "valueId": "eipk",
                    "valueName": "No basic education",
                    "count": 5191,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "prerequisites",
                    "valueId": "kt",
                    "valueName": "Bachelor's degree (University)",
                    "count": 6324,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "prerequisites",
                    "valueId": "pk",
                    "valueName": "Basic education",
                    "count": 7888,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "prerequisites",
                    "valueId": "yo",
                    "valueName": "General upper secondary education and / or matriculation examination",
                    "count": 7473,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "topicFacet": [
            "facetValues": [
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_1",
                    "valueName": "General education",
                    "count": 1007,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_1.aiheet_1",
                            "valueName": "Basic Education",
                            "count": 69,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_1"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_1.aiheet_2",
                            "valueName": "General upper secondary education",
                            "count": 581,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_1"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_1.aiheet_3",
                            "valueName": "other General education (punaisella lisäykset)",
                            "count": 363,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_1"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_10",
                    "valueName": "Art and Design, Music, Theatre ",
                    "count": 906,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_61",
                            "valueName": "Film and Television",
                            "count": 57,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_62",
                            "valueName": "Literature",
                            "count": 104,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_63",
                            "valueName": "Conservation",
                            "count": 3,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_64",
                            "valueName": "Visual arts",
                            "count": 144,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_65",
                            "valueName": "Crafts and design",
                            "count": 134,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_66",
                            "valueName": "Media Production",
                            "count": 107,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_67",
                            "valueName": "Design",
                            "count": 114,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_68",
                            "valueName": "Music",
                            "count": 253,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_69",
                            "valueName": "Cirkus",
                            "count": 8,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_70",
                            "valueName": "Art and Design",
                            "count": 119,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_71",
                            "valueName": "Theatre and dance",
                            "count": 161,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_72",
                            "valueName": "Textiles and clothing",
                            "count": 15,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_10.aiheet_73",
                            "valueName": "Other education in culture",
                            "count": 80,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_10"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_11",
                    "valueName": "Technology and Engineering",
                    "count": 1734,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_74",
                            "valueName": "Architecture and construction, landscape planning",
                            "count": 339,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_75",
                            "valueName": "Food sciences, food industry and biotechnology",
                            "count": 83,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_76",
                            "valueName": "Graphics and communications technology",
                            "count": 29,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_77",
                            "valueName": "Mechanical, metal and energy engineering",
                            "count": 265,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_78",
                            "valueName": "Land surveing",
                            "count": 15,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_79",
                            "valueName": "Materials Technology and Surface Engineering",
                            "count": 28,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_80",
                            "valueName": "Software technology",
                            "count": 114,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_81",
                            "valueName": "Process, Chemistry and materials engineering ",
                            "count": 199,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_82",
                            "valueName": "Electrical and automation engineering",
                            "count": 260,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_83",
                            "valueName": "Research and development",
                            "count": 53,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_84",
                            "valueName": "Information and telecommunications technology",
                            "count": 339,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_85",
                            "valueName": "Research and development (R&D)",
                            "count": 88,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_86",
                            "valueName": "Environmental Engineering",
                            "count": 115,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_11.aiheet_87",
                            "valueName": "Other education in technology, communications and transport",
                            "count": 270,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_11"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_12",
                    "valueName": "Health care, Well-being, Nutrition, Medicine and Pharmacy",
                    "count": 1443,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_100",
                            "valueName": "Other education in social services, health and sports",
                            "count": 208,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_88",
                            "valueName": "Veterinary Medicine",
                            "count": 12,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_89",
                            "valueName": "Pharmacy and other pharmacological services",
                            "count": 60,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_90",
                            "valueName": "Dentistry and other dental services",
                            "count": 38,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_91",
                            "valueName": "Beauty care",
                            "count": 77,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_92",
                            "valueName": "Rehabilitation and sports",
                            "count": 174,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_93",
                            "valueName": "Medicine",
                            "count": 164,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_94",
                            "valueName": "Optometry",
                            "count": 2,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_95",
                            "valueName": "Social services",
                            "count": 212,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_96",
                            "valueName": "Health care and social services",
                            "count": 401,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_97",
                            "valueName": "Technical health services",
                            "count": 21,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_98",
                            "valueName": "Health",
                            "count": 670,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_12.aiheet_99",
                            "valueName": "Nutrition",
                            "count": 106,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_12"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_13",
                    "valueName": "Security, Fire and Rescue Services",
                    "count": 7,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_13.aiheet_101",
                            "valueName": "Fire and rescue services",
                            "count": 4,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_13"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_13.aiheet_102",
                            "valueName": "Police services",
                            "count": 2,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_13"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_13.aiheet_103",
                            "valueName": "Military and border guard",
                            "count": 2,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_13"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_13.aiheet_104",
                            "valueName": "Correctional services",
                            "count": 1,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_13"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_14",
                    "valueName": "Agriculture, Forestry and Environmental Sciences",
                    "count": 383,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_105",
                            "valueName": "Sustainable development",
                            "count": 78,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_106",
                            "valueName": "Nature and the environment",
                            "count": 105,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_107",
                            "valueName": "Agriculture",
                            "count": 105,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_108",
                            "valueName": "Forestry",
                            "count": 78,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_109",
                            "valueName": "Horticulture",
                            "count": 41,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_110",
                            "valueName": "Fishery",
                            "count": 9,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_14.aiheet_111",
                            "valueName": "Other education in technology, communications and transport",
                            "count": 72,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_14"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_2",
                    "valueName": "Modern Languages, Information, Communication and Media",
                    "count": 754,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_10",
                            "valueName": "Communication",
                            "count": 211,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_11",
                            "valueName": "Languages, general linguistics and language technology",
                            "count": 73,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_4",
                            "valueName": "Information and communication",
                            "count": 95,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_5",
                            "valueName": "Library and Information services",
                            "count": 22,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_6",
                            "valueName": "Cognitive science",
                            "count": 4,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_7",
                            "valueName": "Speech Sciences",
                            "count": 41,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_8",
                            "valueName": "Interpreting",
                            "count": 34,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_2.aiheet_9",
                            "valueName": "Languages",
                            "count": 489,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_2"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_3",
                    "valueName": "History, Folklore and Cultural Studies",
                    "count": 656,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_12",
                            "valueName": "Anthropology",
                            "count": 13,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_13",
                            "valueName": "History and archaeology",
                            "count": 93,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_14",
                            "valueName": "Ethnology",
                            "count": 12,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_15",
                            "valueName": "Philosophy",
                            "count": 66,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_16",
                            "valueName": "Folklore",
                            "count": 11,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_17",
                            "valueName": "Cultural and arts research",
                            "count": 124,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_18",
                            "valueName": "Museology",
                            "count": 18,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_19",
                            "valueName": "Theology",
                            "count": 85,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_20",
                            "valueName": "Comparative Religion",
                            "count": 17,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_3.aiheet_21",
                            "valueName": "Other education in humanities and education",
                            "count": 340,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_3"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_4",
                    "valueName": "Social Sciences and Law",
                    "count": 757,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_22",
                            "valueName": "Regional studies",
                            "count": 12,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_23",
                            "valueName": "Administration",
                            "count": 100,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_24",
                            "valueName": "Development studies",
                            "count": 13,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_25",
                            "valueName": "Law",
                            "count": 343,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_26",
                            "valueName": "Political sciences",
                            "count": 70,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_27",
                            "valueName": "Social sciences",
                            "count": 212,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_28",
                            "valueName": "Gender Studies",
                            "count": 21,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_29",
                            "valueName": "Political science",
                            "count": 33,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_4.aiheet_30",
                            "valueName": "Other education in social sciences, business and administration",
                            "count": 140,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_4"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_5",
                    "valueName": "Business, Economics and Finance",
                    "count": 1414,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_31",
                            "valueName": "Management",
                            "count": 450,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_32",
                            "valueName": "Economics",
                            "count": 86,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_33",
                            "valueName": "Business and commerce",
                            "count": 1020,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_34",
                            "valueName": "Marketing",
                            "count": 342,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_35",
                            "valueName": "Sales",
                            "count": 171,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_36",
                            "valueName": "Industrial Engineering and Management",
                            "count": 91,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_5.aiheet_37",
                            "valueName": "Entrepreneurship",
                            "count": 254,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_5"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_6",
                    "valueName": "Transport Services and administration",
                    "count": 273,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_6.aiheet_38",
                            "valueName": "Automotive and transport engineering",
                            "count": 226,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_6"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_6.aiheet_39",
                            "valueName": "Transport services",
                            "count": 5,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_6"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_6.aiheet_40",
                            "valueName": "Air service",
                            "count": 2,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_6"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_6.aiheet_41",
                            "valueName": "Logistics",
                            "count": 37,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_6"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_6.aiheet_42",
                            "valueName": "Seafaring",
                            "count": 17,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_6"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_7",
                    "valueName": "Natural Sciences",
                    "count": 763,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_43",
                            "valueName": "Biochemistry",
                            "count": 65,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_44",
                            "valueName": "Biology",
                            "count": 131,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_45",
                            "valueName": "Physics",
                            "count": 133,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_46",
                            "valueName": "Geosciences and astronomy",
                            "count": 25,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_47",
                            "valueName": "Chemistry",
                            "count": 140,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_48",
                            "valueName": "Geography",
                            "count": 36,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_49",
                            "valueName": "Mathematics",
                            "count": 168,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_50",
                            "valueName": "Computer science",
                            "count": 214,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_51",
                            "valueName": "Statistics",
                            "count": 46,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_7.aiheet_52",
                            "valueName": "Other education in natural sciences",
                            "count": 169,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_7"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_8",
                    "valueName": "Education, Psychology and other Behavioural Sciences",
                    "count": 1229,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_8.aiheet_53",
                            "valueName": "Teaching and education",
                            "count": 776,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_8"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_8.aiheet_54",
                            "valueName": "Educational sciences and psychology",
                            "count": 794,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_8"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_8.aiheet_55",
                            "valueName": "Leisure activities and youth work",
                            "count": 115,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_8"
                        ]
                    ],
                    "parentId": ""
                ],
                [
                    "facetField": "theme_ffm",
                    "valueId": "teemat_9",
                    "valueName": "Tourism, Catering, Domestic services and Consumer economics",
                    "count": 504,
                    "childValues": [
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_9.aiheet_56",
                            "valueName": "Food sciences",
                            "count": 13,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_9"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_9.aiheet_57",
                            "valueName": "Tourism",
                            "count": 123,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_9"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_9.aiheet_58",
                            "valueName": "Domestic and consumer services, consumer economics",
                            "count": 8,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_9"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_9.aiheet_59",
                            "valueName": "Hotel and catering",
                            "count": 262,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_9"
                        ],
                        [
                            "facetField": "topic_ffm",
                            "valueId": "teemat_9.aiheet_60",
                            "valueName": "Cleaning services",
                            "count": 145,
                            "childValues": CourseResponseMock.emptyJsonValue,
                            "parentId": "teemat_9"
                        ]
                    ],
                    "parentId": ""
                ]
            ]
        ],
        "articleContentTypeFacet": [
            "facetValues": CourseResponseMock.emptyJsonValue,
            ],
        "providerTypeFacet": [
            "facetValues": CourseResponseMock.emptyJsonValue,
        ],
        "fotFacet": [
            "facetValues": [
                [
                    "facetField": "formOfTeaching_ffm",
                    "valueId": "opetuspaikkakk_1",
                    "valueName": "Contact teaching",
                    "count": 7958,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "formOfTeaching_ffm",
                    "valueId": "opetuspaikkakk_2",
                    "valueName": "Distance teaching",
                    "count": 2987,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "timeOfTeachingFacet": [
            "facetValues": [
                [
                    "facetField": "timeOfTeaching_ffm",
                    "valueId": "opetusaikakk_1",
                    "valueName": "Day time teaching",
                    "count": 7015,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "timeOfTeaching_ffm",
                    "valueId": "opetusaikakk_2",
                    "valueName": "Evening teaching",
                    "count": 1743,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "timeOfTeaching_ffm",
                    "valueId": "opetusaikakk_3",
                    "valueName": "Weekend teaching",
                    "count": 1104,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "formOfStudyFacet": [
            "facetValues": [
                [
                    "facetField": "formOfStudy_ffm",
                    "valueId": "opetusmuotokk_1",
                    "valueName": "Supervised learning",
                    "count": 7027,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "formOfStudy_ffm",
                    "valueId": "opetusmuotokk_2",
                    "valueName": "Independent learning",
                    "count": 3951,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "formOfStudy_ffm",
                    "valueId": "opetusmuotokk_3",
                    "valueName": "Blended learning",
                    "count": 2514,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "formOfStudy_ffm",
                    "valueId": "opetusmuotokk_4",
                    "valueName": "Online studies",
                    "count": 2455,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "applicationSystemFacet": [
            "facetValues": [
                [
                    "facetField": "asFacet_ffm",
                    "valueId": "1.2.246.562.29.70000333388",
                    "valueName": "Joint application to higher education spring 2019",
                    "count": 1535,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ],
                [
                    "facetField": "asFacet_ffm",
                    "valueId": "1.2.246.562.29.99771464296",
                    "valueName": "Perusopetuksen jälkeisen valmistavan koulutuksen kevään 2019 haku",
                    "count": 142,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "siirtohakuFacet": [
            "facetValues": [
                [
                    "facetField": "siirtohaku_ffm",
                    "valueId": "true",
                    "valueName": "",
                    "count": 700,
                    "childValues": CourseResponseMock.emptyJsonValue,
                    "parentId": ""
                ]
            ]
        ],
        "lopRecommendationFilter": [
                "facetField": "",
                "valueId": "true",
                "valueName": "",
                "count": -1,
                "childValues": CourseResponseMock.emptyJsonValue,
                "parentId": ""
        ],
        "loCount": 10595,
        "articleCount": 0,
        "orgCount": 4854,
        "educationCodeRecommendationFilter": [
            "facetField": "",
            "valueId": "true",
            "valueName": "",
            "count": -1,
            "childValues": CourseResponseMock.emptyJsonValue,
            "parentId": ""
        ]
    ]
}
