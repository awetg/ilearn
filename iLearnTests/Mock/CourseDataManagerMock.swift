//
//  CourseDataManagerMock.swift
//  iLearnTests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
@testable import iLearn

class CourseDataManagerMock: CourseDataManagerProtocol {
    
    // internal variables
    private (set) var recievedSearchTerm: String?
    
    // mock data
    var mockSearchResult: CourseSearchResultDTO?
    var mockError: Error?
    
    func search(searchTerm: String, queryParams: [[String : String]]?, completionHandler: @escaping (CourseSearchResultDTO?, Error?) -> ()) {
        recievedSearchTerm = searchTerm
        if let results = mockSearchResult {
            completionHandler(results, nil)
        } else {
            completionHandler(nil, mockError)
        }
        
    }
}
