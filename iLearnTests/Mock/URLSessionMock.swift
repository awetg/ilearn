//
//  URLSessionMock.swift
//  iLearnTests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import Foundation
@testable import iLearn


// This mock is created to test resume is being called on network urlsession task

class URLSessionMock: URLSessionProtocol {
    
    // internal variables
    // must be private (set) to ensure it is recived value
    private (set) var recievedRequset: URLRequest?
    
    // mock data
    var mockData: Data?
    var mockURLResponse: URLResponse?
    var mockError: Error?
    var mockURLSessionDataTask: URLSessionDataTaskProtocol?
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTaskProtocol {
        
        recievedRequset = request
        
        completionHandler(mockData, mockURLResponse, mockError)
        
        return mockURLSessionDataTask ?? URLSessionDataTaskMock()
    }
}

class URLSessionDataTaskMock: URLSessionDataTaskProtocol {
    
    private(set) var resumeCalled = false
    
    func resume() {
        resumeCalled = true
    }
}
