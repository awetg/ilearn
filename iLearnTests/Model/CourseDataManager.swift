//
//  CourseDataManager.swift
//  iLearnTests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import iLearn

class CourseDataManagerTest: XCTestCase {
    
    let networkClient = NetworkclientMock(baseUrl: "test.com")
    
    let failureResult = Response.Result<Data>.failure(NetworkError.badResponse)
    
    let req = Request(endpoint: nil, pathParams: nil)
    
    // expected values
    var error: Error?
    var searchResults: CourseSearchResultDTO?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        self.networkClient.mockResponse = nil
        self.error = nil
        self.searchResults = nil
    }
    
    func testReturnsErrorForEmptySearchTerm() {
        
         // testing async operation with expectation
        let expectation = self.expectation(description: "testReturnsErrorForEmptySearchTerm")
        
        let courseDataManager = CourseDataManager(netWorkClient: networkClient)
        
        // expected values
        var error: Error?
        var searchResults: CourseSearchResultDTO?
        
        // search with empty search term
        courseDataManager.search(searchTerm: "", queryParams: nil) { (courses, err) in
            if let err = err {
                error = err
            }
            
            if let results = courses {
                searchResults = results
            }
            
            expectation.fulfill()
        }
        
        // wait until expectaion is fullfilled
        XCTWaiter().wait(for: [expectation], timeout: 1.0)
        
        // check that error is not nill
        XCTAssertNotNil(error)
        
        // check that search result is nil
        XCTAssertNil(searchResults)
    }
    
    func testReturnsCourseSearchDTOForValidResponse() {
        
        // testing async operation with expectation
        let expectation = self.expectation(description: "testReturnsCoursesForValidResponse")
        
        // mock api response of course search results
        let data = CourseResponseMock.searchResultMock.jsonString().data(using: .utf8)
        let successResult =  Response.Result<Data>.success(data ?? Data())
        networkClient.mockResponse = Response(httpResponse: nil, request: req, data: data, result: successResult)
        
        // check that file was read and response is not nil
        XCTAssertNotNil(networkClient.mockResponse)
        
        let courseDataManager = CourseDataManager(netWorkClient: networkClient)
        
        courseDataManager.search(searchTerm: "*", queryParams: nil) { (courses, err) in
            if let err = err {
                self.error = err
            }
            
            if let results = courses {
                self.searchResults = results
            }
            
            expectation.fulfill()
        }
        
        // wait until expectaion is fullfilled
        XCTWaiter().wait(for: [expectation], timeout: 1.0)
        
        // check that there is not error when network returns valid response
        XCTAssertNil(error)
        
        // check that retured result is not nil
        XCTAssertNotNil(searchResults)
        
        // check that returned response is equal with mock data
        // need to implement Equatable for CourseSearchResultDTO to test equality. it will be implemented in the futur
    }
    
    func testCourseManagerCascadesNetworkError() {
        
        // testing async operation with expectation
        let expectation = self.expectation(description: "testCascadesNetworkError")
        
        // mock failed network client response
        networkClient.mockResponse = Response(httpResponse: nil, request: req, data: nil, result: failureResult)
        
        // check response it not nil
        XCTAssertNotNil(networkClient.mockResponse)
        
        let courseDataManager = CourseDataManager(netWorkClient: networkClient)
        
        courseDataManager.search(searchTerm: "*", queryParams: nil) { (courses, err) in
            if let err = err {
                self.error = err
            }
            
            if let results = courses {
                self.searchResults = results            }
            
            expectation.fulfill()
        }
        
        // wait until expectaion is fullfilled
        XCTWaiter().wait(for: [expectation], timeout: 1.0)
        
        // check course data manager cascade server or network clinet error
        XCTAssertNotNil(error)
        
        // check that search result is nil
        XCTAssertNil(searchResults)
        
    }

}
