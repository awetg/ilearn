//
//  CoreDataManger.swift
//  iLearnTests
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
import CoreData
@testable import iLearn

class CoreDataMangerTest: XCTestCase {
    
    var coreDataManager: CoreDataManager?
    
    var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        return managedObjectModel
    }()
    
    // create mock persitant container to use in memory store type for unit test
    lazy var mockPersistantContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "iLearnTest", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == NSInMemoryStoreType )
            
            if let error = error {
                fatalError("ERROR: Can't create in memory container \(error)")
            }
        }
        return container
    }()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        coreDataManager = CoreDataManager(container: mockPersistantContainer)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testStoreIsEmpty() {
        // using topic facet values to test if store is empty
        // fetchAll function could be added to test all types of facet values
        let topicFacetValues = coreDataManager?.fetchTopicFacetValues()
        
        // check that 0 facet values are returned
        XCTAssertEqual(topicFacetValues?.count, 0)
    }
    
    func testInsertingFacetValues() {
        
        // check that facet store is empty first
        let topicFacetVAluesBefore = coreDataManager?.fetchTopicFacetValues()
        XCTAssertEqual(topicFacetVAluesBefore?.count, 0)
        
        // prepare mock network response data to insert into store
        let data = CourseResponseMock.searchResultMock.jsonString().data(using: .utf8)
        let searchResultDTO = try? JSONDecoder().decode(CourseSearchResultDTO.self, from: data ?? Data())
        
        // insert mock data to store
        coreDataManager?.insertFacetValues(facetValues: searchResultDTO?.topicFacet?.facetValues ?? [])
        coreDataManager?.save()
        
        // check that facet values are inserted
        let topicFacetValuesAfter = coreDataManager?.fetchTopicFacetValues()
        XCTAssertEqual(topicFacetValuesAfter?.count, searchResultDTO?.topicFacet?.facetValues.count)
    }


}
