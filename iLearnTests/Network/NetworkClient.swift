//
//  NetworkClient.swift
//  iLearnTests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import iLearn

class NetworkClientTest: XCTestCase {
    
    let urlSession = URLSessionMock()
    
    let req = Request(endpoint: nil, pathParams: nil)
    
    // expected values
    var error: Error? = nil
    var response: ResponseProtocol? = nil

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        self.urlSession.mockURLResponse = nil
        self.urlSession.mockData = nil
        self.error = nil
        self.response = nil
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInvalidBaseUrl() {
        
        let networkClient = NetworkClient(baseUrl: "#####", urlSession: urlSession)
        
        networkClient.execute(req) { res in }
        
        // Check that URLSession recieved a nill Request
        // Reques object will be nill if URL is nill
        XCTAssertNil(urlSession.recievedRequset)
    }
    
    func testReturnsFailurResultForInvalidData() {
        
        // testing async operation with expectation
        let expectation = self.expectation(description: "testReturnsErrorforInvalidJSONData")
        
        // mock invalid json response from server
        urlSession.mockData = "Invalid data".data(using: .utf8)
        
        // mock httpUrlResponse with valid status code
        urlSession.mockURLResponse = HTTPURLResponse(url: URL(string: "test.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        
        // check data is not null
        XCTAssertNotNil(urlSession.mockData)
        
        // check urlResponse is not null
        XCTAssertNotNil(urlSession.mockURLResponse)
        
        // expected error value for testing type of error as well
        var error: NetworkError? = nil
        
        let networkClient = NetworkClient(baseUrl: "test.com", urlSession:urlSession)
        
        networkClient.execute(req) { (res) in
            
            switch res.result {
            case .success(_): self.response = res
            case .failure(let err):
                // Casting to Network error since invalid json should only produce Network error
                error = err as? NetworkError
                self.response = res
            }
            
            expectation.fulfill()
        }
        
        // wait until expectaion is fullfilled
         XCTWaiter().wait(for: [expectation], timeout: 1.0)
        
        // check that there is error and is equal to network error bad response
         XCTAssertEqual(error, NetworkError.badResponse)
        
        // check that json data is returned and is equal to invalid mock json
        // network client will still return the invalid json but the response will be failure type of response
        XCTAssertEqual(urlSession.mockData, response?.data)
        
    }
    
    func testReturnsSuccessResultForvalidData() {
        
        // testing async operation with expectation
        let expectation = self.expectation(description: "testReturnsSuccessforValidJSONData")
        
        // mock valid json data e.g ["key": "value"]
        urlSession.mockData = try? JSONSerialization.data(withJSONObject: ["key": "value"], options: [])
        
        // mock httpUrlResponse  with valid status code
        urlSession.mockURLResponse = HTTPURLResponse(url: URL(string: "test.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        
        // check data is not null
        XCTAssertNotNil(urlSession.mockData)
        
        // check urlResponse is not null
        XCTAssertNotNil(urlSession.mockURLResponse)
        
        let networkClient = NetworkClient(baseUrl: "test.com", urlSession:urlSession)
        
        networkClient.execute(req) { (res) in
            
            switch res.result {
            case .success(_): self.response = res
            case .failure(let err): self.error = err
            }
            
            expectation.fulfill()
        }
        
        // wait until expectaion is fullfilled
        XCTWaiter().wait(for: [expectation], timeout: 1.0)
        
        // check that there is not error since the json data is valid
        XCTAssertNil(error)
        
        // check that json data is returned not null
        // check also the json has the passed key and value
        let json = try? JSONSerialization.jsonObject(with: response?.data ?? Data(), options: [.allowFragments]) as? [String: Any]
        XCTAssertEqual(json??["key"] as? String, "value")
        
    }
    
    func testResumeIsCalled() {

        // get URLSessionDataTaskProtocl to get URLSessionDataTask returned from URLSession.dataTask()
        let task = URLSessionDataTaskMock()
        
        // set URLSessionDataTask of URLSession this will be returned from urlsession.dataTask()
        urlSession.mockURLSessionDataTask = task
        
        XCTAssertNotNil(urlSession.mockURLSessionDataTask)
        
        let networkClient = NetworkClient(baseUrl: "test.com", urlSession: urlSession)
        
        networkClient.execute(req) { res in }
        
        // Check that resume was called on URLSession.dataTask() of NetworkClient
        XCTAssertTrue(task.resumeCalled)
    }

}
