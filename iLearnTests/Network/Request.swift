//
//  Request.swift
//  iLearnTests
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 iosdev. All rights reserved.
//

import XCTest
@testable import iLearn

class RequestTest: XCTestCase {
    
    // expected values
    var error: Error? = nil
    var response: ResponseProtocol? = nil

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInvalidEndPoint() {
        
        // testing async operation with expectation
        let expectation = self.expectation(description: "testReturnsErrorforInvalidJSONData")
        
        // value of url is inside NetworkClient that recieve the request
        // we will need to create network client with valid base url for testing end point
        let req = Request(endpoint: "#####", pathParams: nil)
        let urlSession = URLSessionMock()
        let networkClient = NetworkClient(baseUrl: "test.com", urlSession: urlSession)
        
        networkClient.execute(req) { (res) in
            
            switch res.result {
            case .success(_): self.response = res
            case .failure(let err): self.error = err
            }
            
            expectation.fulfill()
        }
        
        // wait until expectaion is fullfilled
        XCTWaiter().wait(for: [expectation], timeout: 1.0)
        
        XCTAssertNil(urlSession.recievedRequset)
        
    }

}
